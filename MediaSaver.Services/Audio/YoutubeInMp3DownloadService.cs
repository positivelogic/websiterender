﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

using MediaSaver.Entities;
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Library.Extensions;
using System.Linq;

namespace MediaSaver.Services
{
    //based on the web resuest : http://youtubeinmp3.com/fetch/?video={0}

    public class YoutubeInMp3DownloadService : IDownloadService
    {
        const string baseUrl = "http://youtubeinmp3.com";
        readonly HttpClient _client;
        public YoutubeInMp3DownloadService(int timeout, long maxResponseContentBufferSize)
        {
            _client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(timeout),
                BaseAddress = new Uri(baseUrl),
                MaxResponseContentBufferSize = maxResponseContentBufferSize
            };
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public event DownloadHandler OnDownloadProgress;

        public async Task SaveAudioAsync(string link, string path, CancellationTokenSource cancelToken, string tubeTitle = null)
        {
            var resource = $"fetch/?format=JSON&video={link}";

            var response = await _client.GetAsync(resource, cancelToken.Token);

            if (response == null || response.Content == null)
                throw new ApplicationException($"Request of resource {resource} failed.");

            if (response.IsSuccessStatusCode)
            {
                var jsonValue = await response.Content.ReadAsStringAsync();
                var youtubeInMp3Response = Newtonsoft.Json.JsonConvert.DeserializeObject<YoutubeInMp3Response>(jsonValue);
                if (youtubeInMp3Response == null)
                    throw new ApplicationException($"Request of resource {resource} failed with invalid response object.");

                var downloadResourceArray = youtubeInMp3Response.link.Split('/');
                var downloadResource = $"{downloadResourceArray[3]}/{downloadResourceArray[4]}/{downloadResourceArray[5]}";
                var downloadResponse = await _client.GetAsync(downloadResource, cancelToken.Token);
                const int artistIndex = 0;
                const int titleIndex = 1;

                if (downloadResponse.IsSuccessStatusCode)
                {
                    var artistName = string.Empty;
                    var title = string.Empty;
                    var mp3name = string.Empty;
                    var splitArray = new string[] { };

                    if (!string.IsNullOrEmpty(tubeTitle))
                    {
                        artistName = tubeTitle.RemoveIllegalChars().RemoveUnwantedCharacters().Trim();
                        mp3name = $"{artistName}";
                        splitArray = tubeTitle.Split('-');
                    }
                    else
                    {
                        artistName = youtubeInMp3Response.title.RemoveIllegalChars().RemoveUnwantedCharacters().Trim();
                        mp3name = $"{artistName}";
                        splitArray = youtubeInMp3Response.title.Split('-');
                    }

                    if (splitArray.ElementAtOrDefault(artistIndex) != null)
                    {
                        artistName = splitArray[artistIndex].RemoveIllegalChars().Trim();
                        mp3name = $"{artistName}";
                    }

                    if (splitArray.ElementAtOrDefault(titleIndex) != null)
                    {
                        title = splitArray[titleIndex].RemoveIllegalChars().RemoveUnwantedCharacters().Trim();
                        mp3name = $"{artistName} - {title}";
                    }


                    var pathToSaveAudio = Path.Combine(path, artistName).ToTitleCase();
                    if (!Directory.Exists(pathToSaveAudio))
                        Directory.CreateDirectory(pathToSaveAudio);

                    var filename = Path.Combine(pathToSaveAudio, $"{mp3name}.mp3");
                    var mp3FileStream = await downloadResponse.Content.ReadAsStreamAsync();

                    using (var fileStream = File.Create(filename))
                    {
                        mp3FileStream.Seek(0, SeekOrigin.Begin);
                        mp3FileStream.CopyTo(fileStream);
                        mp3FileStream.Dispose();
                    }
                }
                else
                {
                    throw new ApplicationException($"Request of resource {downloadResource} failed with statuscode {response.StatusCode}.");
                }
            }
            else
            {
                throw new ApplicationException($"Request of resource {resource} failed with statuscode {response.StatusCode}.");
            }
        }

        public Task SaveVideoAsync(string link, string path, int resolution)
        {
            throw new NotImplementedException();
        }
    }
}