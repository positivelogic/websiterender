﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using YoutubeExtractor;

using MediaSaver.Entities;
using MediaSaver.Library.Extensions;
using MediaSaver.Shared.StorageManagement;

namespace MediaSaver.Services
{
    public class YoutubeExtractorDownloadService : IDownloadService
    {
        public event DownloadHandler OnDownloadProgress;

        public async Task SaveVideoAsync(string link, string path, int resolution)
        {
            await Task.Run(() =>
            {
                IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link);

                var requiredResolution = (int)resolution;

                //Get the maximum resolution available
                var availableResolutions = videoInfos.Where(info => info.VideoType == VideoType.Mp4).Select(m=>m.Resolution).Distinct().ToList();

                //No resolutions available, we cannot continue
                if (availableResolutions == null) throw new ApplicationException("No resolutions available.");

                //Required resolution not available, we cannot continue. Advise the caller of other options available. 
                if (!availableResolutions.Exists(m => m == requiredResolution))
                {
                    string availableResolutionAsString = string.Join(",", availableResolutions.ToArray());
                    throw new ApplicationException(string.Format("Resolution of {0} selected is not available.\r\nAvailable resolution options {1}", 
                        requiredResolution, availableResolutionAsString));
                }

                /*
                 * Select the first .mp4 video with supplied resolution
                 */
                VideoInfo video = videoInfos.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == requiredResolution);

                /*
                 * If the video has a decrypted signature, decipher it
                 */
                if (video.RequiresDecryption)
                {
                    DownloadUrlResolver.DecryptDownloadUrl(video);
                }

                /*
                 * Create the video downloader.
                 * The first argument is the video to download.
                 * The second argument is the path to save the video file.
                 */
                var cleanedFilename = video.Title.RemoveIllegalChars();
                var videoDownloader = new VideoDownloader(video, Path.Combine(path, cleanedFilename + video.VideoExtension));

                // Register the ProgressChanged event and print the current progress
                videoDownloader.DownloadProgressChanged += (sender, args) => FireProgressEventToConsumersThatSubscribed(args.ProgressPercentage);

                /*
                 * Execute the video downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                videoDownloader.Execute();
            });
        }

        void FireProgressEventToConsumersThatSubscribed(double progressPercentage)
        {
            OnDownloadProgress?.Invoke(this, new DownloadArgs { ProgressPercentage = progressPercentage });
        }

        public async Task SaveAudioAsync(string link, string path, CancellationTokenSource cancelSource, string tubeTitle = null)
        {
            await Task.Run(() =>
            {



                IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link);
                /*
                 * We want the first extractable video with the highest audio quality.
                 */
                VideoInfo video = videoInfos
                    .Where(info => info.CanExtractAudio)
                    .OrderByDescending(info => info.AudioBitrate)
                    .FirstOrDefault();

                if (video == null) throw new ApplicationException(string.Format("Cannot save {0} as audio", link));

                /*
                 * If the video has a decrypted signature, decipher it
                 */
                if (video.RequiresDecryption)
                {
                    DownloadUrlResolver.DecryptDownloadUrl(video);
                }

                /*
                 * Create the audio downloader.
                 * The first argument is the video where the audio should be extracted from.
                 * The second argument is the path to save the audio file.
                 */
                var cleanedFilename = video.Title.RemoveIllegalChars();
                var audioDownloader = new AudioDownloader(video, Path.Combine(path, cleanedFilename + video.AudioExtension));

                // Register the progress events. We treat the download progress as 85% of the progress and the extraction progress only as 15% of the progress,
                // because the download will take much longer than the audio extraction.
                audioDownloader.DownloadProgressChanged += (sender, args) => Console.WriteLine(args.ProgressPercentage * 0.85);
                audioDownloader.AudioExtractionProgressChanged += (sender, args) => Console.WriteLine(85 + args.ProgressPercentage * 0.15);

                /*
                 * Execute the audio downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                audioDownloader.Execute();
            });
        }
    }
}