﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

using VideoLibrary;

using MediaSaver.Entities;
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Library.Extensions;

namespace MediaSaver.Services
{
    public class VideoLibraryDownloadServices : IDownloadService
    {
        public event DownloadHandler OnDownloadProgress;

        public Task SaveAudioAsync(string link, string path, CancellationTokenSource cancelToken, string tubeTitle = null)
        {
            throw new NotImplementedException();
        }

        public async Task SaveVideoAsync(string link, string path, int resolution)
        {
            await Task.Run(async () =>
            {
                var youTube = YouTube.Default;                      // starting point for YouTube actions
                var video = await youTube.GetVideoAsync(link);      // gets a Video object with info about the video

                

                //var bytes = video.GetBytes();

                var cleanedFilename = video.FullName.RemoveIllegalChars();

                var videoFile = Path.Combine(path, cleanedFilename + video.FileExtension);

                File.WriteAllBytes(cleanedFilename, video.GetBytes());

                //using (var writer = new BinaryWriter(System.IO.File.Open(path, FileMode.Create)))) {

                //    // do your read logic here and read into bytes

                //    var bytesLeft = bytes.Length;
                //    var bytesWritten = 0;
                //    while (bytesLeft > 0)
                //    {
                //        int chunk = Math.Min(64, bytesLeft);
                //        writer.WriteBytes(array, bytesWritten, chunk);
                //        bytesWritten += chunk;
                //        bytesLeft -= chunk;

                //        // calculate progress bar status
                //        backgroundWorker.ReportProgress(bytesWritten * 100 / array.Length);
                //    }
                //}


            });
        }

        void FireProgressEventToConsumersThatSubscribed(double progressPercentage)
        {
            OnDownloadProgress?.Invoke(this, new DownloadArgs { ProgressPercentage = progressPercentage });
        }
    }
}