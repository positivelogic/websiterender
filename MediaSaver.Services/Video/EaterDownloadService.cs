﻿using MediaSaver.Shared.StorageManagement;
using System;
using System.Threading.Tasks;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.IO;
using MediaSaver.Entities;

namespace MediaSaver.Services
{
    public class EaterDownloadService : IDownloadService
    {
        const string baseUrl = "http://xxxeater.com";
        readonly HttpClient _client;

        readonly int _timeout;
        long _maxResponseContentBufferSize;
        public EaterDownloadService(int timeout, long maxResponseContentBufferSize)
        {
            _timeout = timeout;
            _maxResponseContentBufferSize = maxResponseContentBufferSize;

            _client = new HttpClient()
            {
                Timeout = TimeSpan.FromSeconds(_timeout),
                BaseAddress = new Uri(baseUrl),
                MaxResponseContentBufferSize = _maxResponseContentBufferSize
            };
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public event DownloadHandler OnDownloadProgress;

        public async Task SaveAudioAsync(string link, string path, CancellationTokenSource cancelToken, string tubeTitle = null)
        {
            await Task.Run(() =>
            {
                throw new NotImplementedException();
            });
        }

        public async Task SaveVideoAsync(string link, string path, int resolution)
        {
            var resource = $"download/?url={link}";

            var response = await _client.GetAsync(resource);

            if (response == null || response.Content == null)
                throw new ApplicationException($"Request of resource {resource} failed.");

            var downloadResource = link;

            if (response.IsSuccessStatusCode)
            {
                var htmlStream = await response.Content.ReadAsStreamAsync();

                List<string> found = new List<string>();
                string line;
                string indentifier = "var video = ";
                using (StreamReader file = new StreamReader(htmlStream))
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        if (line.Contains(indentifier))
                        {
                            found.Add(line);
                            break;
                        }
                    }
                }
                if (found.Count > 0)
                {
                    var rawValue = found[0].Split(';');
                    var jsonValue = rawValue[0].Replace(indentifier, string.Empty).Trim();
                    var mediaList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Info>>(jsonValue);
                    if (mediaList != null && mediaList.Count > 0)
                    {
                        var firstMediaItem = mediaList[0];
                        if (firstMediaItem.link.Length > 0)
                        {
                            var firstLink = firstMediaItem.link[0];
                            var url = firstLink.url;
                            using (var client = new HttpClient())
                            {
                                client.Timeout = TimeSpan.FromMinutes(10);
                                var mediaLinkResponse = await client.GetAsync(url);
                                if (mediaLinkResponse.IsSuccessStatusCode)
                                {
                                    if (!Directory.Exists(path))
                                        Directory.CreateDirectory(path);

                                    var filename = Path.Combine(path, $"{firstMediaItem.title}.{firstLink.mime_short}");
                                    var videoFileStream = await mediaLinkResponse.Content.ReadAsStreamAsync();

                                    using (var fileStream = File.Create(filename))
                                    {
                                        videoFileStream.Seek(0, SeekOrigin.Begin);
                                        videoFileStream.CopyTo(fileStream);
                                        videoFileStream.Dispose();
                                    }
                                }
                                else
                                {
                                    throw new ApplicationException($"Request of resource {downloadResource} failed with statuscode {response.StatusCode}.");
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                throw new ApplicationException($"Request of resource {downloadResource} failed with statuscode {response.StatusCode}.");
            }
        }
    }
}