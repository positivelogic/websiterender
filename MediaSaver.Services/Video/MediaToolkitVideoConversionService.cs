﻿using System.Threading.Tasks;

using MediaToolkit;
using MediaToolkit.Model;

namespace MediaSaver.Services
{
    public class MediaToolkitVideoConversionService : Shared.UI.IVideoConversionService
    {
        public async Task ConvertAsync(string inputFile, string outputFile)
        {
            await Task.Run(() =>
            {
                using (var engine = new Engine())
                {
                    var mediaInput = new MediaFile(inputFile);
                    var mediaOutput = new MediaFile(outputFile);
                    engine.Convert(mediaInput, mediaOutput);
                }
            });
        }
    }
}