﻿using System;

using NLog;

using MediaSaver.Shared.Instrumentation;

namespace MediaSaver.Services
{
    public class LoggingService : ILoggingService
    {
        ILogger _logger;
        public LoggingService()
        {
            _logger = LogManager.GetLogger(GetType().FullName);
        }

        public void LogInfo(string message)
        {
            _logger.Info(message);
        }
        public void LogDebug(string message)
        {
            _logger.Debug(message);
        }
        public void LogError(Exception ex, string message = "")
        {
            _logger.Error(ex, message);
        }
    }
}