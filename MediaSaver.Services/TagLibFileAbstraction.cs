﻿using System.IO;

namespace MediaSaver.Services
{
    internal class SimpleFile
    {
        public SimpleFile(string Name, Stream Stream)
        {
            this.Name = Name;
            this.Stream = Stream;
        }
        public string Name { get; set; }
        public Stream Stream { get; set; }
    }


    internal class TagLibFileAbstraction : TagLib.File.IFileAbstraction
    {
        private SimpleFile _file;
        public TagLibFileAbstraction(SimpleFile file)
        {
            _file = file;
        }

        public string Name
        {
            get { return _file.Name; }
        }

        public System.IO.Stream ReadStream
        {
            get { return _file.Stream; }
        }

        public System.IO.Stream WriteStream
        {
            get { return _file.Stream; }
        }

        public void CloseStream(System.IO.Stream stream)
        {
            stream.Position = 0;
        }
    }
}