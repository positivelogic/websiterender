﻿### Add	SearchNext method
	public override void SearchNext()
	{
		if (CanParse)
		{
			CQ doc = CQ.CreateFromUrl(NextPage);
			var links = doc[SelectedSiteInfo.SearchResultFilter];

			VideoUrls.Clear();
			var counter = 1;
			foreach (IDomObject link in links)
			{
				var attr = link[SelectedSiteInfo.CoreAttribute];
				var title = link[SelectedSiteInfo.CoreDescription1];
				var fullUrl = string.Format("{0}{1}", BaseUrl, attr);
				if (!VideoUrls.Any(m => m.Url == fullUrl) && (!string.IsNullOrEmpty(title)))
				{
					var videoUrl = new VideoUrl { Number = counter, Url = fullUrl, Title = title };
					VideoUrls.Add(videoUrl);
					counter++;
				}
			}

			NextPage = null;
			PageNumber = null;
			var pagingNext = doc[SelectedSiteInfo.SearchNextResultFilter];
			foreach (IDomObject page in pagingNext)
			{
				NextPage = page[SelectedSiteInfo.CoreAttribute];
				if (!string.IsNullOrEmpty(NextPage))
				{
					PageNumber = NextPage.Split('&').LastOrDefault();
				}
			}


			PrevPage = null;
			var pagingPrev = doc[SelectedSiteInfo.SearchPrevResultFilter];
			foreach (IDomObject page in pagingPrev)
			{
				PrevPage = page[SelectedSiteInfo.CoreAttribute];
			}
		}
	}

### Add SearchPrev method
    public override void SearchPrev()
    {
        if (CanParse)
        {
            CQ doc = CQ.CreateFromUrl(PrevPage);
            var links = doc[SelectedSiteInfo.SearchResultFilter];

            VideoUrls.Clear();
            var counter = 1;
            foreach (IDomObject link in links)
            {
                var attr = link[SelectedSiteInfo.CoreAttribute];
                var title = link[SelectedSiteInfo.CoreDescription1];
                var fullUrl = string.Format("{0}{1}", BaseUrl, attr);
                if (!VideoUrls.Any(m => m.Url == fullUrl) && (!string.IsNullOrEmpty(title)))
                {
                    var videoUrl = new VideoUrl { Number = counter, Url = fullUrl, Title = title };
                    VideoUrls.Add(videoUrl);
                    counter++;
                }
            }

            NextPage = null;
            PageNumber = null;
            var pagingNext = doc[SelectedSiteInfo.SearchNextResultFilter];
            foreach (IDomObject page in pagingNext)
            {
                NextPage = page[SelectedSiteInfo.CoreAttribute];
            }


            PrevPage = null;
            var pagingPrev = doc[SelectedSiteInfo.SearchPrevResultFilter];
            foreach (IDomObject page in pagingPrev)
            {
                PrevPage = page[SelectedSiteInfo.CoreAttribute];
                if (!string.IsNullOrEmpty(PrevPage))
                {
                    PageNumber = PrevPage.Split(SelectedSiteInfo.MorePageSplitter).LastOrDefault();
                }
            }
        }
    }

#### Removed from Search method

                NextPage = null;
                PageNumber = null;
                var pagingNext = doc[SelectedSiteInfo.SearchNextResultFilter];
                foreach (IDomObject page in pagingNext)
                {
                    NextPage = page[SelectedSiteInfo.CoreAttribute];
                    if (!string.IsNullOrEmpty(NextPage))
                    {
                        PageNumber = NextPage.Split(SelectedSiteInfo.MorePageSplitter).LastOrDefault();
                    }
                }

                PrevPage = null;
                var pagingPrev = doc[SelectedSiteInfo.SearchPrevResultFilter];
                foreach (IDomObject page in pagingPrev)
                {
                    PrevPage = page[SelectedSiteInfo.CoreAttribute];
                    if (!string.IsNullOrEmpty(PrevPage))
                    {
                        PageNumber = PrevPage.Split(SelectedSiteInfo.MorePageSplitter).LastOrDefault();
                    }
                }
### Add these Properties


        string _prevPage;
        public string PrevPage { get { return _prevPage; }
            set
            {
                _prevPage = value;
                OnPropertyChanged();
                Prev = !string.IsNullOrEmpty(_prevPage);
                OnPropertyChanged("Prev");
            }
        }

        string _nextPage;
        public string NextPage { get { return _nextPage; }
            set
            {
                _nextPage = value;
                OnPropertyChanged();
                Next = !string.IsNullOrEmpty(_nextPage);
                OnPropertyChanged("Next");
            }
        }

##### Execute an action via polly
    var policy = Policy
        .Handle<System.Net.WebException>()
        .Retry(1);

    var result = policy.ExecuteAndCapture(() => _searchServices.Search(SelectedSiteInfo, FormattedUrl));

    if (result.Outcome == OutcomeType.Successful)
    {
        var videoUrls = result.Result;
        foreach (var videoUrl in videoUrls)
        {
            VideoUrls.Add(videoUrl);
        }
    }
    else
    {
        throw result.FinalException;
    }
