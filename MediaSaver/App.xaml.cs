﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Diagnostics;
using System.Reflection;

using CefSharp;
using TwoLayeredGUI;
using TwoLayeredGUI.WPF;
using Unity;
using Unity.Lifetime;
using Unity.Injection;

using MediaSaver.Services;
using MediaSaver.Views;
using MediaSaver.ViewModels;
using MediaSaver.Library.Proxy;
using MediaSaver.Library.Shell;

//.NET Standard
using MediaSaver.Shared.Instrumentation;
using MediaSaver.Shared.UI;
using MediaSaver.Shared.KeyManagement;
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Services.Client;
using MediaSaver.Entities;

namespace MediaSaver
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, ISingleInstanceApp
    {
        private const string Unique = "{CBB1AA19-9E10-481E-B5E9-8A62FF7690B6}";
        public static UnityContainer Container;
        static ILoggingService _logger;
        [STAThread]
        public static void Main()
        {
            if (SingleInstance<App>.InitializeAsFirstInstance(Unique))
            {
                Trace.CorrelationManager.ActivityId = Guid.NewGuid();

                Container = CreateUnityContainer();

                _logger = Container.Resolve<ILoggingService>();

                _logger.LogInfo("Starting application");
                var app = new App();

                _logger.LogInfo("Setup resource dictionaries.");
                //MahApps.Metro resource dictionaries. Make sure that all file names are Case Sensitive!
                var controls = new Uri("pack://application:,,,/MahApps.Metro;component/Styles/Controls.xaml", UriKind.RelativeOrAbsolute);
                var controlRes = new ResourceDictionary() { Source = controls };
                app.Resources.MergedDictionaries.Add(controlRes);

                var fonts = new Uri("pack://application:,,,/MahApps.Metro;component/Styles/Fonts.xaml", UriKind.RelativeOrAbsolute);
                var fontsRes = new ResourceDictionary() { Source = fonts };
                app.Resources.MergedDictionaries.Add(fontsRes);

                var colors = new Uri("pack://application:,,,/MahApps.Metro;component/Styles/Colors.xaml", UriKind.RelativeOrAbsolute);
                var colorsRes = new ResourceDictionary() { Source = colors };
                app.Resources.MergedDictionaries.Add(colorsRes);

                _logger.LogInfo("Setup themes.");
                //Accent and AppTheme setting
                var accentBlue = new Uri("pack://application:,,,/MahApps.Metro;component/Styles/Accents/Blue.xaml", UriKind.RelativeOrAbsolute);
                var accentBlueRes = new ResourceDictionary() { Source = accentBlue };
                app.Resources.MergedDictionaries.Add(accentBlueRes);

                var accentBaseLight = new Uri("pack://application:,,,/MahApps.Metro;component/Styles/Accents/BaseLight.xaml", UriKind.RelativeOrAbsolute);
                var accentBaseLightRes = new ResourceDictionary() { Source = accentBaseLight };
                app.Resources.MergedDictionaries.Add(accentBaseLightRes);

                try
                {
                    if (app.Dispatcher != null)
                        app.Dispatcher.UnhandledException += Dispatcher_UnhandledException;

                    _logger.LogInfo("Setting Cef values");
                    var settings = new CefSettings
                    {
                        PersistSessionCookies = true,
                        PersistUserPreferences = true,
                        WindowlessRenderingEnabled = true,
                        MultiThreadedMessageLoop = true,
                        IgnoreCertificateErrors = true,
                        LogSeverity = LogSeverity.Warning,
                        //The location where cache data will be stored on disk. If empty an in-memory cache will be used for some features and a temporary disk cache for others.
                        //HTML5 databases such as localStorage will only persist across sessions if a cache path is specified. 
                        CachePath = "cache"
                    };

                    _logger.LogInfo("Setting proxy server");
                    var proxy = ProxyConfig.GetProxyInformation();
                    switch (proxy.AccessType)
                    {
                        case InternetOpenType.Direct:
                            {
                                //Don't use a proxy server, always make direct connections.
                                settings.CefCommandLineArgs.Add("no-proxy-server", "1");
                                break;
                            }
                        case InternetOpenType.Proxy:
                            {
                                settings.CefCommandLineArgs.Add("proxy-server", proxy.ProxyAddress);
                                break;
                            }
                        case InternetOpenType.PreConfig:
                            {
                                settings.CefCommandLineArgs.Add("proxy-auto-detect", "1");
                                break;
                            }
                    }

                    _logger.LogInfo(string.Format("Proxy server set to {0}", proxy.AccessType));


                    settings.CefCommandLineArgs.Add("debug-plugin-loading", "1");   //Dumps extra logging about plugin loading to the log file.
                                                                                    //settings.CefCommandLineArgs.Add("enable-system-flash", "1");  //Automatically discovered and load a system-wide installation of Pepper Flash.

                    settings.FocusedNodeChangedEnabled = true;

                    // Initialize cef with the provided settings
                    if (!Cef.IsInitialized)
                    {
                        if (!Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null))
                            throw new Exception("Unable to Initialize Cef");
                        else
                            _logger.LogInfo("Cef initialised.");
                    }

                    var mainWindowView = Container.Resolve<MainWindowView>();
                    _logger.LogInfo("Showing main window view as dialog.");
                    mainWindowView.ShowDialog();
                }
                catch (Exception ex)
                {
                    var mesageBoxProvider = Container.Resolve<ICustomMessageBoxService>();
                    mesageBoxProvider.ShowError(ex);
                }
                finally
                {
                    if (app.Dispatcher != null)
                        app.Dispatcher.UnhandledException -= Dispatcher_UnhandledException;

                    if (Cef.IsInitialized)
                    {
                        _logger.LogInfo("Shutting down cef.");
                        Cef.Shutdown();
                    }

                    app.Shutdown();
                    _logger.LogInfo("Shutting down app.");


                    // Allow single instance code to perform cleanup operations
                    SingleInstance<App>.Cleanup();
                    _logger.LogInfo("Cleanup single instance.");

                    _logger = null;
                    // Fastest way, but this is something like kernel panic
                    Process.GetCurrentProcess().Kill();

                }
            }
        }

        static bool UseVideoLibrary = true;
        public static UnityContainer CreateUnityContainer()
        {
            var container = new UnityContainer();
            container.RegisterType<ILoggingService, LoggingService>(new ContainerControlledLifetimeManager());  //Register a type to have a singleton lifetime without mapping the type
            container.RegisterType<IApiKeyProviderService, ApiKeyProviderService>();
            container.RegisterType<ISearchServices, SearchServices>(new InjectionConstructor(
                                                                        new ResolvedParameter<IDownloadService>(IocEnums.VideoLibrary), 
                                                                        new ResolvedParameter<IVideoConversionService>(),
                                                                        new ResolvedParameter<IDownloadService>(IocEnums.YoutubeInMp3),
                                                                        new ResolvedParameter<ILoggingService>()
                                                                    ));


            var codebase = Assembly.GetExecutingAssembly().CodeBase;
            container.RegisterType<ISiteInfoService, SiteInfoService>(new InjectionConstructor(codebase));
            container.RegisterType<IMessageBoxProvider, MessageBoxProvider>();
            container.RegisterType<ICustomMessageBoxService, CustomMessageBoxService>();
            container.RegisterType<IThreadingServices, ThreadingServices>();


            container.RegisterType<IDownloadService, VideoLibraryDownloadServices>(name: IocEnums.VideoLibrary);
            container.RegisterType<IDownloadService, YoutubeExtractorDownloadService>(name: IocEnums.YoutubeExtractor);

            container.RegisterType<IYoutubeApiSearchService, YoutubeApiSearchService>(new ContainerControlledLifetimeManager());
            container.RegisterType<IVideoConversionService, MediaToolkitVideoConversionService>();
            var httpClientTimeInSeconds = 60;
            long httpClientMaxResponseContentBufferSize = 9999999;
            container.RegisterType<IDownloadService, YoutubeInMp3DownloadService>(IocEnums.YoutubeInMp3, new ContainerControlledLifetimeManager(), new InjectionConstructor(httpClientTimeInSeconds, httpClientMaxResponseContentBufferSize));
            container.RegisterType<IFileService, FileService>();

            container.RegisterType<IMainVm, MainVm>(new ContainerControlledLifetimeManager(), new InjectionConstructor(
                                                                                                new ResolvedParameter<ISiteInfoService>(),
                                                                                                new ResolvedParameter<ISearchServices>(),
                                                                                                new ResolvedParameter<IThreadingServices>(),
                                                                                                new ResolvedParameter<ILoggingService>(),
                                                                                                new ResolvedParameter<IDownloadService>(IocEnums.YoutubeExtractor),
                                                                                                new ResolvedParameter<IYoutubeApiSearchService>(),
                                                                                                new ResolvedParameter<IVideoConversionService>(),
                                                                                                new ResolvedParameter<IDownloadService>(IocEnums.YoutubeInMp3)
                                                                                            ));
            container.RegisterType<IFilesVm, FilesVm>();
            container.RegisterType<ISiteInfoVm, SiteInfoVm>();
            container.RegisterType<ISettingsVm, SettingsVm>();

            return container;
        }

        static void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            var mesageBoxProvider = Container.Resolve<ICustomMessageBoxService>();
            mesageBoxProvider.ShowError(e.Exception);
        }

        public bool SignalExternalCommandLineArgs(IList<string> args)
        {
            return true;
        }
    }
}