﻿using System.Collections.Generic;
using System.Linq;

using CsQuery;
using Unity;

using MediaSaver.ViewModels;
using MediaSaver.Entities;
using MediaSaver.Library.Extensions;
//.NET Standard
using MediaSaver.Shared.UI;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Shared.Instrumentation;
using MediaSaver.Shared.StorageManagement;

namespace MediaSaver.Services.Client
{
    public interface ISearchServices
    {
        IEnumerable<VideoUrlVm> Search(SiteInfo siteInfo, string url);
    }

    public class SearchServices : ISearchServices
    {
        readonly IDownloadService _youtubeDownloadService;
        readonly IVideoConversionService _videoConversionService;
        readonly IDownloadService _youtubeInMp3DownloadServices;
        readonly ILoggingService _loggingService;
        public SearchServices(IDownloadService youtubeDownloadService, 
                                IVideoConversionService videoConversionService,
                                    IDownloadService youtubeInMp3DownloadServices,
                                        ILoggingService loggingService)
        {
            _youtubeDownloadService = youtubeDownloadService;
            _videoConversionService = videoConversionService;
            _youtubeInMp3DownloadServices = youtubeInMp3DownloadServices;
            _loggingService = loggingService;
        }

        public IEnumerable<VideoUrlVm> Search(SiteInfo siteInfo, string url)
        {
            var videoUrls = new List<VideoUrlVm>();

            CQ doc = null;

            doc = CQ.CreateFromUrl(url);

            if (doc != null)
            {
                var links = doc[siteInfo.SearchResultFilter];
                videoUrls.Clear();
                var counter = 1;
                foreach (IDomObject link in links)
                {
                    var attr = link[siteInfo.CoreAttribute];
                    var title = link[siteInfo.CoreDescription1];
                    var fullUrl = string.Format("{0}{1}", siteInfo.BaseUrl, attr);
                    if (!videoUrls.Any(m => m.Url == fullUrl) && (!string.IsNullOrEmpty(title)))
                    {
                        var parent = App.Container.Resolve<IMainVm>();
                        var setting = App.Container.Resolve<ISettingsVm>();
                        var videoUrl = new VideoUrlVm(_youtubeDownloadService, _videoConversionService, _youtubeInMp3DownloadServices, parent, setting, _loggingService)
                        {
                            Number = counter,
                            Url = fullUrl,
                            Title = title.RemoveIllegalChars()
                        };
                        videoUrls.Add(videoUrl);
                        counter++;
                    }
                }
            }

            return videoUrls;
        }
    }
}