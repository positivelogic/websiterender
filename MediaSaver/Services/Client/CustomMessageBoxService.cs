﻿using System;
using TwoLayeredGUI;
using TwoLayeredGUI.GUIProviderExtension;

//.NET Standard
using MediaSaver.Shared.Instrumentation;
using MediaSaver.Shared.UI;

namespace MediaSaver.Services.Client
{
    public class CustomMessageBoxService : ICustomMessageBoxService
    {
        readonly IMessageBoxProvider _messageBoxProvider;
        readonly ILoggingService _loggingService;
        public CustomMessageBoxService(IMessageBoxProvider messageBoxProvider, ILoggingService loggingService)
        {
            _messageBoxProvider = messageBoxProvider;
            _loggingService = loggingService;
        }

        public void ShowError(Exception ex)
        {
            _loggingService.LogError(ex);
            _messageBoxProvider.ShowError(ex);
        }
    }
}