﻿using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace MediaSaver.Services.Client
{
    public interface IThreadingServices
    {
        Dispatcher Dispatcher { get; }
        SynchronizationContext SynchronizationContext { get; }
    }

    public class ThreadingServices : IThreadingServices
    {
        public Dispatcher Dispatcher
        {
            get
            {
                if (Application.Current != null)
                    if (Application.Current.Dispatcher != null)
                        return Application.Current.Dispatcher;
                return null;
            }
        }

        public SynchronizationContext SynchronizationContext
        {
            get
            {
                if (SynchronizationContext.Current != null)
                    return SynchronizationContext.Current;
                return new SynchronizationContext();
            }
        }
    }
}