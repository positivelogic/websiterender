﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using Unity;

using MediaSaver.ViewModels;
using MediaSaver.Library.Extensions;

//.NET Standard
using MediaSaver.Shared.KeyManagement;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Shared.UI;
using MediaSaver.Shared.Instrumentation;
using MediaSaver.Entities;
using MediaSaver.Shared.StorageManagement;

namespace MediaSaver.Services.Client
{
    public interface IYoutubeApiSearchService
    {
        Task<IEnumerable<VideoUrlVm>> SearchAsync(string query);
        string PreviousToken { get; set; }
        string NextToken { get; set; }
        string DesiredToken { get; set; }
        int? ResultsPerPage { get; set; }
        int? TotalResults { get; set; }
        int? ItemCounter { get; set; }
        int? CurrentPage { get; set; }
        int? TotalPages { get; set; }
    }

    public class YoutubeApiSearchService : IYoutubeApiSearchService
    {
        readonly ISettingsVm _settings;
        readonly IApiKeyProviderService _apiKeyProviderService;
        readonly ILoggingService _loggingService;
        public YoutubeApiSearchService(IApiKeyProviderService apiKeyProviderService, 
                                         ISettingsVm settings,
                                            ILoggingService loggingService)
        {
            _apiKeyProviderService = apiKeyProviderService;
            _settings = settings;
            _loggingService = loggingService;
            CurrentPage = null;
        }

        public string PreviousToken { get; set; }
        public string NextToken { get; set; }
        public string DesiredToken { get; set; }
        public int? ResultsPerPage { get; set; }
        public int? TotalResults { get; set; }
        public int? ItemCounter { get; set; }
        public int? CurrentPage { get; set; }
        public int? TotalPages { get; set; }

        public async Task<IEnumerable<VideoUrlVm>> SearchAsync(string query)
        {
            var videoUrls = new List<VideoUrlVm>();
            var apiKey = _apiKeyProviderService.GetYoutubeApiKey();
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = apiKey,
                ApplicationName = this.GetType().ToString()
            });

            
            var searchListRequest = youtubeService.Search.List("snippet");
            searchListRequest.PageToken = DesiredToken;
            searchListRequest.Q = query;
            searchListRequest.MaxResults = _settings.MaxResults;
            searchListRequest.Type = "video";
            searchListRequest.VideoDefinition = SearchResource.ListRequest.VideoDefinitionEnum.Any;
            searchListRequest.VideoType = SearchResource.ListRequest.VideoTypeEnum.Any;

            // Call the search.list method to retrieve results matching the specified query term.
            var searchListResponse = await searchListRequest.ExecuteAsync();

            NextToken = searchListResponse.NextPageToken;
            PreviousToken = searchListResponse.PrevPageToken;

            ResultsPerPage = searchListResponse.PageInfo.ResultsPerPage;
            TotalResults = searchListResponse.PageInfo.TotalResults;

            TotalPages = TotalResults / ResultsPerPage;

            // Add each result to the appropriate list, and then display the lists of matching videos, channels, and playlists.
            foreach (var searchResult in searchListResponse.Items)
            {
                switch (searchResult.Id.Kind)
                {
                    case "youtube#video":
                        var youtubeDownloadService = App.Container.Resolve<IDownloadService>(IocEnums.VideoLibrary);
                        var videoConversionService = App.Container.Resolve<IVideoConversionService>();
                        var youtubeInMp3DownloadServices = App.Container.Resolve<IDownloadService>(IocEnums.YoutubeInMp3);
                        var parent = App.Container.Resolve<IMainVm>();
                        var setting = App.Container.Resolve<ISettingsVm>();
                        var url = _settings.YouTubeApiBaseurl;
                        videoUrls.Add(new VideoUrlVm(youtubeDownloadService, videoConversionService, youtubeInMp3DownloadServices, parent, setting, _loggingService)
                        {
                            Number = ItemCounter.GetValueOrDefault(),
                            Title = searchResult.Snippet.Title.RemoveIllegalChars(),
                            Url = string.Format(url, searchResult.Id.VideoId)
                        });
                        ItemCounter++;
                        break;

                    case "youtube#channel":
                        //channels.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.ChannelId));
                        break;

                    case "youtube#playlist":
                        //playlists.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.PlaylistId));
                        break;
                }
            }

            return videoUrls;
        }
    }
}