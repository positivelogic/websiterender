﻿using System;

using MahApps.Metro.Controls;
using Unity.Attributes;

using MediaSaver.ViewModels;

//.NET Standard
using MediaSaver.Shared.UI;

namespace MediaSaver.Views
{
    /// <summary>
    /// Interaction logic for FileView.xaml
    /// </summary>
    public partial class FileView : MetroWindow, IView
    {
        public FileView()
        {
            InitializeComponent();
        }

        [Dependency]
        public IFilesVm ViewModel
        {
            set
            {
                DataContext = value;
                (DataContext as FilesVm).View = this as IView;
            }
        }

        public object Me { get { return this; } }

        public void CloseMe()
        {
            Close();
        }

        public void Exit()
        {
            throw new NotImplementedException();
        }

        public void RefreshMe()
        {
            throw new NotImplementedException();
        }
    }
}
