﻿using System;

using MahApps.Metro.Controls;
using Unity.Attributes;

using MediaSaver.ViewModels;
//.NET Standard
using MediaSaver.Shared.UI;
using MediaSaver.Shared.ConfigurationManagement;

namespace MediaSaver.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : MetroWindow, IView
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        public object Me { get { return this; } }

        [Dependency]
        public ISettingsVm ViewModel
        {
            set
            {
                DataContext = value;
                (DataContext as SettingsVm).View = this as IView;
            }
        }

        public void CloseMe()
        {
            Close();
        }

        public void Exit()
        {
            throw new NotImplementedException();
        }

        public void RefreshMe()
        {
            UpdateLayout();
        }
    }
}
