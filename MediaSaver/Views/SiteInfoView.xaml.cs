﻿using System;

using Unity.Attributes;
using MahApps.Metro.Controls;

using MediaSaver.ViewModels;
//.NET Standard
using MediaSaver.Shared.UI;


namespace MediaSaver.Views
{
    /// <summary>
    /// Interaction logic for SiteInfoView.xaml
    /// </summary>
    public partial class SiteInfoView : MetroWindow, IView
    {
        public SiteInfoView()
        {
            InitializeComponent();
        }

        public object Me { get { return this; } }

        [Dependency]
        public ISiteInfoVm ViewModel
        {
            set
            {
                DataContext = value;
                (DataContext as SiteInfoVm).View = this as IView;
            }
        }

        public void CloseMe()
        {
            Close();
        }

        public void Exit()
        {
            throw new NotImplementedException();
        }

        public void RefreshMe()
        {
            UpdateLayout();
        }
    }
}