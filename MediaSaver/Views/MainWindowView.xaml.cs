﻿using System;
using System.Windows;
using System.Diagnostics;
using System.Windows.Input;

using Unity.Attributes;
using MahApps.Metro.Controls;

using MediaSaver.ViewModels;

//.NET Standard
using MediaSaver.Shared.UI;

namespace MediaSaver.Views
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : MetroWindow, IView
    {
        public MainWindowView()
        {
            InitializeComponent();

            webBrowserChrome.BrowserSettings.Javascript = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.ApplicationCache = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.LocalStorage = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.Plugins = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.RemoteFonts = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.TabToLinks = CefSharp.CefState.Enabled;
            webBrowserChrome.BrowserSettings.WebGl = CefSharp.CefState.Disabled;
            webBrowserChrome.BrowserSettings.WebSecurity = CefSharp.CefState.Enabled;

            webBrowserChrome.ConsoleMessage += WebBrowserChrome_ConsoleMessage;
            webBrowserChrome.IsBrowserInitializedChanged += WebBrowserChrome_IsBrowserInitializedChanged;
        }

        [Dependency]
        public IMainVm ViewModel
        {
            set
            {
                DataContext = value;
                (DataContext as MainVm).View = this as IView;
            }
        }

        public object Me { get { return this; } }

        private void WebBrowserChrome_IsBrowserInitializedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Debug.WriteLine("Chrome|{0}|OldValue:{1}|NewValue:{2}", e.Property.Name, e.OldValue, e.NewValue);
        }

        private void WebBrowserChrome_ConsoleMessage(object sender, CefSharp.ConsoleMessageEventArgs e)
        {
            Debug.WriteLine("Chrome|Line|{0}", e.Line);
            Debug.WriteLine("Chrome|Message|{0}", e.Message);
            Debug.WriteLine("Chrome|Source|{0}", e.Source);
        }

        public void CloseMe()
        {
            Close();
        }

        public void Exit()
        {
            //Fastest way, but this is something like kernel panic
            Process.GetCurrentProcess().Kill();
        }

        public void RefreshMe()
        {
            // Somehow the Ui compenents does not get updated. Had to add this here, so the refresh of compenents can be called
            // from other threads. Not ideal
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal,
                                   new Action(delegate ()
                                   {
                                       CommandManager.InvalidateRequerySuggested();
                                   }));
        }
    }
}