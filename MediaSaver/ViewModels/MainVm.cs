﻿using System;
using System.Linq;
using System.Windows.Input;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Unity;

using MediaSaver.Services;
using MediaSaver.Library.Input;
using MediaSaver.Views;
using MediaSaver.Library;

//.NET Standard
using MediaSaver.Entities;
using MediaSaver.Shared.UI;
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Shared.Instrumentation;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Services.Client;

namespace MediaSaver.ViewModels
{
    public interface IMainVm
    {
        bool CanSearch { get; }
        string DesiredToken { get; set; }
        ICommand EditSiteInfoCommand { get; }
        ICommand ExitApplicationCommand { get; }
        string FormattedUrl { get; }
        bool IsBusy { get; set; }
        string NextToken { get; set; }
        string PageInformation { get; set; }
        string PreviousToken { get; set; }
        ICommand RefreshSiteInfoCommand { get; }
        ICommand SearchCommand { get; }
        string SearchCriteria { get; set; }
        ICommand SearchNextCommand { get; }
        ICommand SearchPreviousCommand { get; }
        SiteInfo SelectedSiteInfo { get; set; }
        VideoUrlVm SelectedVideoUrl { get; set; }
        ICommand ShowSettingsCommand { get; }
        List<SiteInfo> SiteInfoList { get; set; }
        int? TotalResults { get; set; }
        AsyncObservableCollection<VideoUrlVm> VideoUrls { get; set; }
        IView View { get; set; }
        void Dispose();
        Task SearchAsync();
    }

    public class MainVm : BindingBase, IDisposable, IMainVm
    {
        readonly ISiteInfoService _siteInfoService;
        readonly ISearchServices _searchServices;
        readonly IThreadingServices _dispatcherServices;
        readonly ILoggingService _loggingService;
        readonly IYoutubeApiSearchService _youtubeApiSearchService;
        readonly IDownloadService _youtubeInMp3DownloadServices;
        const string PageInfoFormat = "Page {0} of {1}";
        public MainVm(ISiteInfoService siteInfoService, 
                        ISearchServices searchServices, 
                            IThreadingServices dispatcherServices, 
                                ILoggingService loggingService,
                                    IDownloadService youtubeVideoDownloadService,
                                        IYoutubeApiSearchService youtubeApiSearchService,
                                            IVideoConversionService videoConversionService,
                                                IDownloadService youtubeInMp3DownloadServices)
        {
            _siteInfoService = siteInfoService;
            _searchServices = searchServices;
            _dispatcherServices = dispatcherServices;
            _loggingService = loggingService;
            _youtubeApiSearchService = youtubeApiSearchService;
            _youtubeInMp3DownloadServices = youtubeInMp3DownloadServices;
            try
            {
                IsBusy = false;

                if (_siteInfoService == null)
                    throw new ArgumentNullException("SiteInfoService");

                if (_searchServices == null)
                    throw new ArgumentNullException("SearchServices");

                SearchCriteria = "";

                LoadSiteInfo();

                var sw = new StreamWriter(Console.OpenStandardOutput());
                sw.AutoFlush = true;
                Console.SetOut(sw);

                var dispatcher = _dispatcherServices.Dispatcher;
                var synchronizationContext = _dispatcherServices.SynchronizationContext;
                VideoUrls = new AsyncObservableCollection<VideoUrlVm>(dispatcher, synchronizationContext);
                var setting = App.Container.Resolve<ISettingsVm>();
                SelectedVideoUrl = new VideoUrlVm(youtubeVideoDownloadService, videoConversionService, _youtubeInMp3DownloadServices, this, setting, _loggingService)
                {
                    Number = 1,
                    Url = "about:blank"
                };
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                throw;
            }
        }

        private void SetPageInformation(int? currentPage, int? totalPages)
        {
            PageInformation = string.Format(PageInfoFormat, currentPage, totalPages);
        }
       
        ~MainVm()
        {
            Dispose();
        }

        public IView View { get; set; }

        List<SiteInfo> _siteInfoList;
        public List<SiteInfo> SiteInfoList { get { return _siteInfoList; }
            set
            {
                _siteInfoList = value;
                OnPropertyChanged();
            }
        }

        SiteInfo _selectedSiteInfo;
        public SiteInfo SelectedSiteInfo { get { return _selectedSiteInfo; }
            set
            {
                _selectedSiteInfo = value;
                OnPropertyChanged();
            }
        }

        string _searchCriteria;
        public string SearchCriteria { get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged();
                OnPropertyChanged("CanSearch");
                OnPropertyChanged("FormattedUrl");
            }
        }

        public bool CanSearch
        {
            get
            {
                return (!string.IsNullOrEmpty(SearchCriteria));
            }
        }

        AsyncObservableCollection<VideoUrlVm> _videoUrls;
        public AsyncObservableCollection<VideoUrlVm> VideoUrls { get { return _videoUrls; }
            set
            {
                _videoUrls = value;
                OnPropertyChanged();
            }
        }

        VideoUrlVm _selectedVideoUrl;
        public VideoUrlVm SelectedVideoUrl { get { return _selectedVideoUrl; }
            set
            {
                _selectedVideoUrl = value;
                if (_selectedVideoUrl !=  null)
                    System.Diagnostics.Debug.WriteLine(_selectedVideoUrl.Url);
                OnPropertyChanged();
            }
        }

        string _formattedUrl;
        public string FormattedUrl
        {
            get
            {
                var formattedSearchCriteria = SearchCriteria.Replace(" ", "+");
                var formattedSearchUrl = string.Format(SelectedSiteInfo.SearchUrl, formattedSearchCriteria);
                _formattedUrl = string.Format("{0}{1}", SelectedSiteInfo.BaseUrl, formattedSearchUrl);
                return _formattedUrl;
            }
        }

        /// <summary>
        /// hack for now. need to sort out error displaying
        /// when doing it from different threads
        /// </summary>
        bool _errorOccured;
        bool ErrorOccured
        {
            get { return _errorOccured; }
            set
            {
                _errorOccured = value;
                OnPropertyChanged();
                if (_errorOccured)
                {
                    var msg = new TwoLayeredGUI.WPF.MessageBoxProvider();
                    if (LastException != null)
                    {
                        System.Windows.MessageBox.Show(LastException.Message, "Media Saver", 
                            System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                        LastException = null;
                    }
                    _errorOccured = false;
                }
            }
        }

        Exception LastException { get; set; }

        public async Task SearchAsync()
        {
            await Task.Run(async() =>
            {
                try
                {
                    ErrorOccured = false;
                    if (CanSearch)
                    {
                        IsBusy = true;
                        VideoUrls.Clear();

                        _youtubeApiSearchService.DesiredToken = DesiredToken;

                        var videoUrls = await _youtubeApiSearchService.SearchAsync(SearchCriteria);

                        NextToken = _youtubeApiSearchService.NextToken;
                        PreviousToken = _youtubeApiSearchService.PreviousToken;
                        TotalResults = _youtubeApiSearchService.TotalResults;

                        foreach (var videoUrl in videoUrls)
                        {
                            VideoUrls.Add(videoUrl);
                        }
                        IsBusy = false;

                        var currentPage = _youtubeApiSearchService.CurrentPage;
                        var totalPages = _youtubeApiSearchService.TotalPages;
                        SetPageInformation(currentPage, totalPages);

                        View.RefreshMe();
                    }
                }
                catch (Exception ex)
                {
                    LastException = ex;
                    ErrorOccured = true;
                    IsBusy = false;
                    _loggingService.LogError(ex);
                    throw;
                }
            });
        }

        string _pageInformation;
        public string PageInformation
        {
            get { return _pageInformation; }
            set
            {
                _pageInformation = value;
                OnPropertyChanged();
            }
        }

        private ICommand _searchCommand;
        public ICommand SearchCommand
        {
            get
            {
                return _searchCommand ?? (_searchCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanSearch,
                        ExecuteDelegate = x =>
                        {
                            Task.Run(async () =>
                            {
                                 _youtubeApiSearchService.CurrentPage = 1;
                                _youtubeApiSearchService.ItemCounter = 1;
                                _youtubeApiSearchService.DesiredToken = null;
                                _youtubeApiSearchService.NextToken = null;
                                _youtubeApiSearchService.PreviousToken = null;
                                DesiredToken = null;
                                NextToken = null;
                                PreviousToken = null;
                                await SearchAsync();
                            });
                        }
                });
            }
        }

        int? _totalResults;
        public int? TotalResults { get { return _totalResults; }
            set
            {
                _totalResults = value;
                OnPropertyChanged();
            }
        }

        string _previousToken;
        public string PreviousToken
        {
            get { return _previousToken; }
            set
            {
                _previousToken = value;
                OnPropertyChanged();
                OnPropertyChanged("CanDoPreviousSearch");
            }
        }

        string _nextToken;
        public string NextToken
        {
            get { return _nextToken; }
            set
            {
                _nextToken = value;
                OnPropertyChanged();
                OnPropertyChanged("CanDoNextSearch");
            }
        }

        string _desiredToken;
        public string DesiredToken
        {
            get { return _desiredToken; }
            set
            {
                _desiredToken = value;
                OnPropertyChanged();
            }
        }

        private ICommand _searchPreviousCommand;
        public ICommand SearchPreviousCommand
        {
            get
            {
                return _searchPreviousCommand ?? (_searchPreviousCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanDoPreviousSearch,
                        ExecuteDelegate = x =>
                        {
                            Task.Run(async () =>
                            {
                                _youtubeApiSearchService.CurrentPage--;
                                DesiredToken = PreviousToken;
                                await SearchAsync();
                            });
                        }
                });
            }
        }

        bool CanDoPreviousSearch
        {
            get
            {
                return (!string.IsNullOrEmpty(PreviousToken));
            }
        }

        private ICommand _searchNextCommand;
        public ICommand SearchNextCommand
        {
            get
            {
                return _searchNextCommand ?? (_searchNextCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanDoNextSearch,
                        ExecuteDelegate = x =>
                        {
                            Task.Run(async () =>
                            {
                                _youtubeApiSearchService.CurrentPage++;
                                DesiredToken = NextToken;
                                await SearchAsync();
                            });
                        }
                });
            }
        }

        bool CanDoNextSearch
        {
            get
            {
                return !string.IsNullOrEmpty(NextToken);
            }
        }

        bool CanEditSiteInfo
        {
            get
            {
                return true;
            }
        }

        bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        private ICommand _editSiteInfoCommand;
        public ICommand EditSiteInfoCommand
        {
            get
            {
                return _editSiteInfoCommand ?? (_editSiteInfoCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanEditSiteInfo,
                        ExecuteDelegate = x =>
                        {
                            EditSiteInfo();
                        }
                });
            }
        }

        void EditSiteInfo()
        {
            var searchCritera = SearchCriteria;
            var editSiteInfoView = App.Container.Resolve<SiteInfoView>();
            editSiteInfoView.Owner = (View.Me as System.Windows.Window);
            editSiteInfoView.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            editSiteInfoView.ShowDialog();
            RefreshSiteInfoCommand.Execute(null);
            SearchCriteria = string.Empty;
            SearchCriteria = searchCritera;
        }

        bool CanExitApplication
        {
            get
            {
                return true;
            }
        }

        private ICommand _exitApplicationCommand;
        public ICommand ExitApplicationCommand
        {
            get
            {
                return _exitApplicationCommand ?? (_exitApplicationCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanExitApplication,
                    ExecuteDelegate = x =>
                    {
                        ExitApplication();
                    }
                });
            }
        }

        void ExitApplication()
        {
            View.Exit();
        }

        bool CanRefreshSiteInfoCommand
        {
            get
            {
                return (_siteInfoService != null);
            }
        }

        private ICommand _refreshSiteInfoCommand;
        public ICommand RefreshSiteInfoCommand
        {
            get
            {
                return _refreshSiteInfoCommand ?? (_refreshSiteInfoCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanRefreshSiteInfoCommand,
                    ExecuteDelegate = x =>
                    {
                        LoadSiteInfo();
                    }
                });
            }
        }

        private void LoadSiteInfo()
        {
            SiteInfoList = _siteInfoService.Load();

            if (SiteInfoList == null)
                throw new ArgumentNullException("SiteInfoList");

            if (SiteInfoList.Count == 0)
                throw new ApplicationException("No siteinfo retrieved.");

            var firstActiveSiteInfo = SiteInfoList.FirstOrDefault(m => m.Active == true);
            if (firstActiveSiteInfo == null)
                throw new ArgumentNullException("FirstActiveSiteInfo");

            SelectedSiteInfo = firstActiveSiteInfo;
        }

        bool CanShowSettingsCommand
        {
            get { return true; }
        }

        private ICommand _showSettingsCommand;
        public ICommand ShowSettingsCommand
        {
            get
            {
                return _showSettingsCommand ?? (_showSettingsCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanShowSettingsCommand,
                    ExecuteDelegate = x =>
                    {
                        ShowSettings();
                    }
                });
            }
        }
        void ShowSettings()
        {
            var settingsView = App.Container.Resolve<SettingsView>();
            settingsView.Owner = (View.Me as System.Windows.Window);
            settingsView.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            settingsView.ShowDialog();
        }

        bool CanViewFilesCommand
        {
            get
            {
                return true;
            }
        }

        private ICommand _viewFilesCommand;
        public ICommand ViewFilesCommand
        {
            get
            {
                return _viewFilesCommand ?? (_viewFilesCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanViewFilesCommand,
                    ExecuteDelegate = x =>
                    {
                        ShowFiles();
                    }
                });
            }
        }

        void ShowFiles()
        {
            var fileView = App.Container.Resolve<FileView>();
            fileView.Owner = (View.Me as System.Windows.Window);
            fileView.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
            fileView.ShowDialog();
        }


        public void Dispose()
        {
           if (_siteInfoService !=  null)
                _siteInfoService.Save(SiteInfoList);
        }
    }
}