﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.Windows.Input;

using MediaSaver.Library.Input;
using MediaSaver.Entities;

//.NET Standard
using MediaSaver.Shared.UI;
using MediaSaver.Shared.StorageManagement;


namespace MediaSaver.ViewModels
{
    public interface ISiteInfoVm
    {
        ICommand SaveCommand { get; }
        ObservableCollection<SiteInfo> SiteInfoList { get; set; }
        IView View { get; set; }
    }

    public class SiteInfoVm : BindingBase, ISiteInfoVm
    {
        readonly ISiteInfoService _siteInfoService;
        readonly ICustomMessageBoxService _customMessageBoxService;
        public SiteInfoVm(ISiteInfoService siteInfoService, ICustomMessageBoxService customMessageBoxService)
        {
            _siteInfoService = siteInfoService;
            _customMessageBoxService = customMessageBoxService;
            SelectedSiteInfo = new SiteInfo();
            Load();
        }

        public bool CanSave { get; set; }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return this._saveCommand ?? (this._saveCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanSave,
                    ExecuteDelegate = x =>
                    {
                        Save();
                    }
                });
            }
        }

        string _header;
        public string Header
        {
            get { return _header; }
            set
            {
                _header = value;
                OnPropertyChanged();
            }
        }

        public IView View { get; set; }

        public ObservableCollection<SiteInfo> SiteInfoList { get; set; }

        SiteInfo _selectedSiteInfo;
        public SiteInfo SelectedSiteInfo
        {
            get { return _selectedSiteInfo; }
            set
            {
                _selectedSiteInfo = value;
                OnPropertyChanged();
                Header = string.Format("SiteInfo Editor - {0} - ({1})", _selectedSiteInfo.Name, _selectedSiteInfo.Active);
                OnPropertyChanged("Header");
            }
        }

        void Load()
        {
            try
            {
                SiteInfoList = new ObservableCollection<SiteInfo>();
                var retrievedSiteInfoList = _siteInfoService.Load();
                foreach (var siteInfo in retrievedSiteInfoList)
                {
                    SiteInfoList.Add(siteInfo);
                }
                if (SiteInfoList.Count == 0) throw new ApplicationException("No site info loaded.");
                SelectedSiteInfo = SiteInfoList.FirstOrDefault();

                CanSave = true;
            }
            catch(Exception ex)
            {
                CanSave = false;
                _customMessageBoxService.ShowError(ex);
            }
        }

        void Save()
        {
            try
            {
                if (CanSave)
                {
                    var siteInfoListToSave = SiteInfoList.ToList();
                    _siteInfoService.Save(siteInfoListToSave);
                }
                View.CloseMe();
            }
            catch(Exception ex)
            {
                _customMessageBoxService.ShowError(ex);
            }
        }
    }
}