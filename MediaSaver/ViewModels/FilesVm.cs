﻿using System.Windows.Input;

using MediaSaver.Services.Client;
using MediaSaver.Entities;
using MediaSaver.Library;
using MediaSaver.Library.Input;
//.NET Standard
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Shared.UI;

namespace MediaSaver.ViewModels
{
    public interface IFilesVm
    {
        AsyncObservableCollection<FileInfoModel> Files { get; set; }
        FileInfoModel SelectedFile { get; set; }
        IView View { get; set; }
    }

    public class FilesVm : BindingBase, IFilesVm
    {
        readonly IThreadingServices _dispatcherServices;
        readonly IFileService _fileservice;
        public FilesVm(IThreadingServices dispatcherServices, IFileService fileservice)
        {
            _dispatcherServices = dispatcherServices;
            _fileservice = fileservice;

            var dispatcher = _dispatcherServices.Dispatcher;
            var synchronizationContext = _dispatcherServices.SynchronizationContext;
            Files = new AsyncObservableCollection<FileInfoModel>(dispatcher,synchronizationContext);
            LoadFilesAsync();
        }

        public IView View { get; set; }

        AsyncObservableCollection<FileInfoModel> _files;
        public AsyncObservableCollection<FileInfoModel> Files
        {
            get { return _files; }
            set
            {
                _files = value;
                OnPropertyChanged();
            }
        }

        async void LoadFilesAsync()
        {
            var files = await _fileservice.LoadFileInfoAsync();
            Files.Clear();
            foreach (var file in files)
            {
                Files.Add(file);
            }
        }

        FileInfoModel _selectedFile;
        public FileInfoModel SelectedFile { get { return _selectedFile; }
            set
            {
                _selectedFile = value;
                OnPropertyChanged();
            }
        }

        bool CanCloseCommand
        {
            get
            {
                return (View !=  null);
            }
        }

        private ICommand _closeCommand;
        public ICommand CloseCommand
        {
            get
            {
                return _closeCommand ?? (_closeCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanCloseCommand,
                    ExecuteDelegate = x =>
                    {
                        Close();
                    }
                });
            }
        }

        void Close()
        {
            if (CanCloseCommand)
                View.CloseMe();
        }
    }
}