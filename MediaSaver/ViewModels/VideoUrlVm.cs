﻿using System.IO;
using System.Windows.Input;
using System.Collections.Generic;

using MediaSaver.Services;
using MediaSaver.Library.Input;
using MediaSaver.Entities;
using MediaSaver.Library.Extensions;

//.NET Standard
using MediaSaver.Shared.UI;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Shared.Instrumentation;
using System;
using System.Threading;
using MediaSaver.Shared.StorageManagement;

namespace MediaSaver.ViewModels
{
    public interface IVideoUrlVm
    {
        int? Number { get; set; }
        string Url { get; set; }
        string Title { get; set; }
        double? PercentageProgress { get; set; }
        ICommand SaveVideoCommand { get; }
        ICommand SaveAudioCommand { get; }
        int SelectedResolution { get; set; }
        List<int> ResolutionOptions { get; set; }
    }

    public class VideoUrlVm : BindingBase, IVideoUrlVm
    {
        readonly IDownloadService _youtubeVideoDownloadService;
        readonly IVideoConversionService _videoConversionService;
        readonly IDownloadService _youtubeInMp3DownloadServices;
        readonly IMainVm _parent;
        readonly ISettingsVm _settings;
        readonly ILoggingService _loggingService;
        public VideoUrlVm(IDownloadService youtubeVideoDownloadService,
                            IVideoConversionService videoConversionService,
                                IDownloadService youtubeInMp3DownloadServices,
                                    IMainVm parent,
                                        ISettingsVm settings,
                                         ILoggingService loggingService)
        {
            _youtubeVideoDownloadService = youtubeVideoDownloadService;
            _youtubeVideoDownloadService.OnDownloadProgress += _downloadService_OnDownloadProgress;
            _videoConversionService = videoConversionService;
            _youtubeInMp3DownloadServices = youtubeInMp3DownloadServices;

            _loggingService = loggingService;

            _parent = parent;
            _settings = settings;

            SelectedResolution = settings.DefaultResolution;
            ResolutionOptions = new List<int>();
            ResolutionOptions.AddRange((new int[] { 0, 144, 240, 360, 480, 720, 1080 }));
        }

        public int? Number { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public int SelectedResolution { get; set; }
        public List<int> ResolutionOptions { get; set; }


        double? _percentageProgress;
        public double? PercentageProgress
        {
            get { return _percentageProgress; }
            set
            {
                _percentageProgress = value;
                OnPropertyChanged();
            }
        }

        const string mp4Extension = ".mp4";
        const string mp3Extension = ".mp3";
        bool CanSaveVideoCommand
        {
            get
            {
                if (_settings.AudioOnlyDownload) return false;

                var url = Url.TrimIfNull();
                var validurl = (!string.IsNullOrEmpty(url));
                var inputFile = Path.Combine(_settings.PathToSave, Title.RemoveIllegalChars()) + mp4Extension;
                var fileDoNotExists = !File.Exists(inputFile);
                return (validurl && fileDoNotExists);
            }
        }

        bool CanSaveAudioCommand
        {
            get
            {

                if (_settings.AudioOnlyDownload)
                {
                    try
                    {
                        var title = Title.RemoveIllegalChars().RemoveUnwantedCharacters();
                        var artistName = title.Split('-')[0].Trim();
                        var mp3filename = Path.Combine(_settings.PathToSave, artistName, title) + mp3Extension;
                        return !File.Exists(mp3filename);
                    }
                    catch (Exception ex)
                    {
                        _loggingService.LogError(ex);
                        return false;
                    }
                }

                var inputFile = Path.Combine(_settings.PathToSave, Title.RemoveIllegalChars()) + mp4Extension;
                var outputFile = Path.Combine(_settings.PathToSave, Title.RemoveIllegalChars()) + mp3Extension;
                var inputFileExists = File.Exists(inputFile);
                var outputFileDoesNotExists = !File.Exists(outputFile);

                return (inputFileExists && outputFileDoesNotExists);
            }
        }

        private ICommand _saveVideoCommand;
        public ICommand SaveVideoCommand
        {
            get
            {
                return _saveVideoCommand ?? (_saveVideoCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanSaveVideoCommand,
                    ExecuteDelegate = x =>
                    {
                        SaveVideo();
                    }
                });
            }
        }

        async void SaveVideo()
        {
            if (CanSaveVideoCommand)
            {
                var path = _settings.PathToSave;
                await _youtubeVideoDownloadService.SaveVideoAsync(Url, path, SelectedResolution);
                _parent.View.RefreshMe();
            }
        }

        private ICommand _saveAudioCommand;
        public ICommand SaveAudioCommand
        {
            get
            {
                return _saveAudioCommand ?? (_saveAudioCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanSaveAudioCommand,
                    ExecuteDelegate = x =>
                    {
                        SaveVideoAsAudio();
                    }
                });
            }
        }

        async void SaveVideoAsAudio()
        {
            var cancelSource = new CancellationTokenSource();
            try
            {
                _parent.IsBusy = true;
                var title = Title.RemoveIllegalChars().RemoveUnwantedCharacters();
                var inputFile = Path.Combine(_settings.PathToSave, title) + mp4Extension;
                var outputFile = Path.Combine(_settings.PathToSave, title) + mp3Extension;

                if (_settings.AudioOnlyDownload)
                {
                    await _youtubeInMp3DownloadServices.SaveAudioAsync(Url, _settings.PathToSave, cancelSource, title);
                }
                else
                {
                    await _videoConversionService.ConvertAsync(inputFile: inputFile, outputFile: outputFile);
                }
                _parent.IsBusy = false;
                _parent.View.RefreshMe();
            }
            catch(Exception ex)
            {
                cancelSource.Cancel();
                _loggingService.LogError(ex);
                if (_parent != null)
                {
                    _parent.IsBusy = false;
                    _parent.View.RefreshMe();
                }
                throw ex;
            }
        }

        private void _downloadService_OnDownloadProgress(object source, DownloadArgs args)
        {
            if (args != null)
            {
                PercentageProgress = args.ProgressPercentage;
            }
        }
    }
}
