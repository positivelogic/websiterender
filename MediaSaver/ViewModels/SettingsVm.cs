﻿using System;
using System.Windows.Input;
using MediaSaver.Library.Input;
//.NET Standard
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Shared.UI;

namespace MediaSaver.ViewModels
{
    public class SettingsVm : BindingBase, ISettingsVm
    {
        public string PathToSave
        {
            get => Properties.Settings.Default.PathToSave;
            set
            {
                Properties.Settings.Default.PathToSave = value;
                OnPropertyChanged();
            }
        }

        public int MaxResults
        {
            get => Properties.Settings.Default.MaxResults;
            set
            {
                Properties.Settings.Default.MaxResults = value;
                OnPropertyChanged();
            }
        }

        public int DefaultResolution
        {
            get => Properties.Settings.Default.DefaultResolution;
            set
            {
                Properties.Settings.Default.DefaultResolution = value;
                OnPropertyChanged();
            }
        }

        public string YouTubeApiBaseurl
        {
            get
            {
                if (ShowYoutubeFullScreen)
                    return Properties.Settings.Default.YouTubeApiBaseurlFullScreen;
                else
                    return Properties.Settings.Default.YouTubeApiBaseurl;
            }
            set
            {
                Properties.Settings.Default.YouTubeApiBaseurl = value;
                OnPropertyChanged();
            }
        }

        public bool ShowYoutubeFullScreen
        {
            get => Properties.Settings.Default.ShowYoutubeFullScreen;
            set
            {
                Properties.Settings.Default.ShowYoutubeFullScreen = value;
                OnPropertyChanged();
                OnPropertyChanged("YouTubeApiBaseurl");
            }
        }

        public string ApiKeyLocation
        {
            get => Properties.Settings.Default.ApiKeyLocation;
            set
            {
                Properties.Settings.Default.ApiKeyLocation = value;
                OnPropertyChanged();
            }
        }

        public bool AudioOnlyDownload {
            get => Properties.Settings.Default.AudioOnlyDownload;
            set
            {
                Properties.Settings.Default.AudioOnlyDownload = value;
                OnPropertyChanged();
            }
        }

        public int SettingsViewLabelWidth
        {
            get => Properties.Settings.Default.SettingsViewLabelWidth;
            set
            {
                Properties.Settings.Default.SettingsViewLabelWidth = value;
                OnPropertyChanged();
            }
        }

        public IView View { get; set; }

        bool CanSaveCommand
        {
            get
            {
                return (View != null);
            }
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand
        {
            get
            {
                return this._saveCommand ?? (this._saveCommand = new SimpleCommand
                {
                    CanExecuteDelegate = x => CanSaveCommand,
                    ExecuteDelegate = x =>
                    {
                        Save();
                    }
                });
            }
        }

        public void Save()
        {
            Properties.Settings.Default.Save();
            View.CloseMe();
        }
    }
}