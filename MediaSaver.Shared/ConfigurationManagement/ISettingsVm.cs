﻿using MediaSaver.Shared.UI;

namespace MediaSaver.Shared.ConfigurationManagement
{
    public interface ISettingsVm
    {
        string PathToSave { get; set; }
        int MaxResults { get; set; }
        int DefaultResolution { get; set; }
        string YouTubeApiBaseurl { get; set; }
        string ApiKeyLocation { get; set; }
        void Save();
        bool ShowYoutubeFullScreen { get; set; }
        bool AudioOnlyDownload { get; set; }
        int SettingsViewLabelWidth { get; set; }
        IView View { get; set; }
    }
}