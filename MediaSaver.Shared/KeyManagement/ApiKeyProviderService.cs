﻿namespace MediaSaver.Shared.KeyManagement
{
    public interface IApiKeyProviderService
    {
        string GetYoutubeApiKey();
    }

    public class ApiKeyProviderService : IApiKeyProviderService
    {
        readonly ConfigurationManagement.ISettingsVm _settings;
        const string YouTubeApiKeyFile = "YoutubeApiKey.txt";
        public ApiKeyProviderService(ConfigurationManagement.ISettingsVm settings)
        {
            _settings = settings;
        }
        public string GetYoutubeApiKey()
        {
            var apiKey = System.IO.Path.Combine(_settings.ApiKeyLocation, YouTubeApiKeyFile);
            return System.IO.File.ReadAllText(apiKey);
        }
    }
}