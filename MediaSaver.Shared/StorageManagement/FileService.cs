﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using MediaSaver.Entities;

namespace MediaSaver.Shared.StorageManagement
{
    public interface IFileService
    {
        Task<IEnumerable<FileInfoModel>> LoadFileInfoAsync();
    }

    public class FileService : IFileService
    {
        readonly ConfigurationManagement.ISettingsVm _settings;
        public FileService(ConfigurationManagement.ISettingsVm settings)
        {
            _settings = settings;
        } 

        public async Task<IEnumerable<FileInfoModel>> LoadFileInfoAsync()
        {
            return await Task.Run(() =>
            {
                var results = new List<FileInfoModel>(); 

                var path = _settings.PathToSave;
                var dir = new DirectoryInfo(path);
                var files = dir.GetFiles("*.*", SearchOption.AllDirectories);

                int counter = 1;
                foreach (var file in files)
                {
                    results.Add(new FileInfoModel
                    {
                        Number = counter,
                        Name = file.Name,
                        Length = file.Length,
                        Extension = file.Extension,
                        CreationTime = file.CreationTime,
                        FullName = file.FullName,
                        Path = file.Directory.FullName,
                    });
                    counter++;
                }

                return results;
            });
        }
    }
}