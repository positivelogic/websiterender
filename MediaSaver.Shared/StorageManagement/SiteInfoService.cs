﻿using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

using MediaSaver.Entities;

namespace MediaSaver.Shared.StorageManagement
{
    public interface ISiteInfoService
    {
        List<SiteInfo> Load();
        void Save(List<SiteInfo> siteinfoList);
    }
    public class SiteInfoService : ISiteInfoService
    {
        readonly string _dataLocation;
        public SiteInfoService(string codebase)
        {
            UriBuilder uri = new UriBuilder(codebase);
            string path = Uri.UnescapeDataString(uri.Path);
            _dataLocation = Path.GetDirectoryName(path);
        }

        public List<SiteInfo> Load()
        {
            var siteInfoList = new List<SiteInfo>();
            var jsonFile = string.Format(@"{0}\Data\{1}", _dataLocation, "SiteInfoList.json");
            var json = File.ReadAllText(jsonFile);
            siteInfoList = JsonConvert.DeserializeObject<List<SiteInfo>>(json);
            return siteInfoList;
        }

        public void Save(List<SiteInfo> siteinfoList)
        {
            var json = JsonConvert.SerializeObject(siteinfoList);
            var jsonFile = string.Format(@"{0}\Data\{1}", _dataLocation, "SiteInfoList.json");
            File.WriteAllText(jsonFile, json);
        }
    }
}