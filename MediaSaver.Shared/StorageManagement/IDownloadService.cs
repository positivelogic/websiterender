﻿using System.Threading;
using System.Threading.Tasks;

using MediaSaver.Entities;

namespace MediaSaver.Shared.StorageManagement
{
    public interface IDownloadService
    {
        Task SaveVideoAsync(string link, string path, int resolution);
        Task SaveAudioAsync(string link, string path, CancellationTokenSource cancelToken, string tubeTitle = null);
        event DownloadHandler OnDownloadProgress;
    }

    public delegate void DownloadHandler(object source, DownloadArgs args);
}