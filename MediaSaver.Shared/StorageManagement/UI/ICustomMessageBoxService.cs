﻿using System;

namespace MediaSaver.Shared.UI
{
    public interface ICustomMessageBoxService
    {
        void ShowError(Exception ex);
    }
}