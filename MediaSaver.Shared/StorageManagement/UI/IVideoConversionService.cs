﻿using System.Threading.Tasks;

namespace MediaSaver.Shared.UI
{
    public interface IVideoConversionService
    {
        Task ConvertAsync(string inputFile, string outputFile);
    }
}