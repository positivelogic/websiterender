﻿namespace MediaSaver.Shared.UI
{
    /// <summary>
    /// Prpvide a mechanism to interact with a view from a viewmodel
    /// Not ideal, but a workaround
    /// </summary>
    public interface IView
    {
        void CloseMe();
        void Exit();
        object Me { get; }

        void RefreshMe();
    }
}