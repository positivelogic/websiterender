﻿using System;

namespace MediaSaver.Shared.Instrumentation
{
    public interface ILoggingService
    {
        void LogInfo(string message);
        void LogError(Exception ex, string message = "");
        void LogDebug(string message);
    }
}