![alt text](https://ci.appveyor.com/api/projects/status/7rq1kwdf1rjrv0dt?svg=true "Build status")
[![codecov](https://codecov.io/bb/positivelogic/websiterender/branch/master/graph/badge.svg)](https://codecov.io/bb/positivelogic/websiterender)
# README #
### Media Saver by PositiveLogic ###
* Quick summary
  * Application that parsers, interpret and downloads multimedia contents from websites via rest api calls (i.e Youtube) and saves the contents to location specified in specified format. (mp4 or mp3)
  * Uses async techniques to make concurrent downloads a breeze.

### How do I get set up? ###
* Summary of set up
	*	Edit the SiteInfoList.json to the likes of your needs, which is located in the Data folder.
* Configuration
	*	Ensure that a folder called Data is present where the MediaSaver.exe runs.
	*	Also ensure that the SiteInfoList.json is present in this folder.
    *   To run the application in debugger, ensure to follow the CEF sharp WPF configuration. (Set build configuration to a specific platform - x86 or x64) For more info see following link: https://www.codeproject.com/articles/881315/display-html-in-wpf-and-cefsharp-tutorial-part
* Dependencies
	*	Nuget package manager takes cares of the 3rd party assemblies.
	*	Microsoft.NET framework 4.6.1
    *   Require a youtube api key. (It's free, create one.) https://developers.google.com/youtube/v3/getting-started
* Data configuration
	*	The website repo data is persisted to a json file. (SiteInfoList.json)
    *   The configuration setting is saved to a user settings xml file. Standard .NET user settings mechanism.
* How to run tests
	*	Using nunit.
    *   Looking at using opencover to get code covereage and be able to publish status of code coverage.
* Deployment instructions
	*	XCopy the contents of the bin folder to your folder of choice and run MediaSaver.exe

### Wish list ###
*   Looking at creating a installer for media saver in the near future.
*   Improve async operations so that it does not drain your computers CPU resources. Occurs on bulk downloads.
*   Expand the multimedia repos.

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
* Repo owner or admin : poslogic@gmail.com
