﻿using System.Collections.Generic;
using Moq;
using MediaSaver.Entities;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqSiteInfoService :
        Mock<Shared.StorageManagement.ISiteInfoService>
    {
        public List<SiteInfo> LoadDataToReturn;
        public MoqSiteInfoService()
        {
            LoadDataToReturn = new List<SiteInfo>();
            Setup(m => m.Load())
                .Returns(delegate
                {
                    return LoadDataToReturn;
                });

            Setup(m => m.Save(It.IsAny<List<SiteInfo>>()));
        }
    }
}