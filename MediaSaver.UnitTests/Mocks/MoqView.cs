﻿using Moq;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqView :
        Mock<Shared.UI.IView>
    {
        public MoqView()
        {
            Setup(m => m.CloseMe());
            Setup(m => m.Exit());
            Setup(m => m.RefreshMe());
        }
    }
}