﻿using Moq;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqSettingsVm :
        Mock<Shared.ConfigurationManagement.ISettingsVm>
    {
        public MoqView MoqView;
        public bool AudioOnlyDownload;
        public MoqSettingsVm()
        {
            MoqView = new MoqView();
            SetupGet(m => m.View).Returns(MoqView.Object);
            SetupGet(m => m.PathToSave).Returns("");
            SetupGet(m => m.AudioOnlyDownload).Returns(() =>
            {
                return AudioOnlyDownload;
            });
        }
    }
}