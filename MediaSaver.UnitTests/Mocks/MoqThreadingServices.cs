﻿using System.Threading;
using System.Windows.Threading;

using Moq;

using MediaSaver.Services.Client;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqThreadingServices
        : Mock<IThreadingServices>
    {
        public MoqThreadingServices()
        {
            SetupGet(m => m.Dispatcher).Returns(It.IsAny<Dispatcher>());
            SetupGet(m => m.SynchronizationContext).Returns(It.IsAny<SynchronizationContext>());
        }
    }
}