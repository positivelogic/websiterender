﻿using Moq;
using System.Threading;
using MediaSaver.Services;
using MediaSaver.Shared.StorageManagement;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqYoutubDownloadService
        : Mock<IDownloadService>
    {
        public MoqYoutubDownloadService()
        {
            Setup(m => m.SaveVideoAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()));
            Setup(m => m.SaveAudioAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationTokenSource>(), It.IsAny<string>()));
        }
    }
}