﻿using System;
using Moq;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqLoggingService :
        Mock<Shared.Instrumentation.ILoggingService>
    {
        public MoqLoggingService()
        {
            Setup(m => m.LogInfo(It.IsAny<string>()));
            Setup(m => m.LogDebug(It.IsAny<string>()));
            Setup(m => m.LogError(It.IsAny<Exception>(), It.IsAny<string>()));
        }
    }
}