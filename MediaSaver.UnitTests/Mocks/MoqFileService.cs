﻿using System.Collections.Generic;
using Moq;
using MediaSaver.Entities;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqFileService :
        Mock<Shared.StorageManagement.IFileService>
    {
        public List<FileInfoModel> LoadFileInfoToReturn;
        public MoqFileService()
        {
            LoadFileInfoToReturn = new List<FileInfoModel>();
            Setup(m => m.LoadFileInfoAsync()).ReturnsAsync(() =>
            {
                return LoadFileInfoToReturn;
            });
        }
    }
}