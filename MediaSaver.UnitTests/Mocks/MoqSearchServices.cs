﻿using System.Collections.Generic;

using Moq;

using MediaSaver.Entities;
using MediaSaver.ViewModels;
using MediaSaver.Services.Client;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqSearchServices :
        Mock<ISearchServices>
    {
        public List<VideoUrlVm> SearchDataToReturn;
        public MoqSearchServices()
        {
            SearchDataToReturn = new List<VideoUrlVm>();

            Setup(m => m.Search(It.IsAny<SiteInfo>(), It.IsAny<string>()))
                .Returns(delegate
                {
                    return SearchDataToReturn;
                });
        }
    }
}