﻿using Moq;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqVideoConversionService :
        Mock<Shared.UI.IVideoConversionService>
    {
        public MoqVideoConversionService()
        {
            Setup(m => m.ConvertAsync(It.IsAny<string>(), It.IsAny<string>()));
        }
    }
}