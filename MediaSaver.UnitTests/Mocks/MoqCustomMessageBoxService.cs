﻿using System;
using Moq;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqCustomMessageBoxService :
        Mock<Shared.UI.ICustomMessageBoxService>
    {
        public MoqCustomMessageBoxService()
        {
            Setup(m => m.ShowError(It.IsAny<Exception>()));
        }
    }
}