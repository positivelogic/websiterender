﻿using Moq;
using TwoLayeredGUI;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqMessageBoxProvider :
        Mock<IMessageBoxProvider>
    {
        public MoqMessageBoxProvider()
        {
            bool rem = false;
            Setup(m => m.Show(It.IsAny<MessageBoxSettings>(), out rem));
        }
    }
}