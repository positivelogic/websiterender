﻿using System.Collections.Generic;

using Moq;
using MediaSaver.ViewModels;
using MediaSaver.Services.Client;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqYoutubeApiSearchService :
        Mock<IYoutubeApiSearchService>
    {
        public List<VideoUrlVm> SearchDataToReturn;
        public MoqYoutubeApiSearchService()
        {
            SearchDataToReturn = new List<VideoUrlVm>();

            Setup(m => m.SearchAsync(It.IsAny<string>()))
                .ReturnsAsync(delegate
                {
                    return SearchDataToReturn;
                });
        }
    }
}