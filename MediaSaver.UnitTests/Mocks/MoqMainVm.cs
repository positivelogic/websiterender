﻿using Moq;
using MediaSaver.ViewModels;

namespace MediaSaver.UnitTests.Mocks
{
    internal class MoqMainVm :
        Mock<IMainVm>
    {
        public MoqView MoqView;
        public MoqMainVm()
        {
            MoqView = new MoqView();
            SetupGet(m => m.View).Returns(MoqView.Object);
        }
    }
}