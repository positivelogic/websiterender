﻿using NUnit.Framework;
using MediaSaver.Library.Extensions;

namespace MediaSaver.UnitTests
{
    [TestFixture]
    public class StringExtensionsUnitTests
    {
        [Test]
        public void Call_RemoveUnwantedCharacters_RemovesNumericAndBrackets()
        {
            //Arrange
            var title = "Billy Ocean - Another Day Won't Matter (1981)";

            //Act
            var target = title.RemoveUnwantedCharacters();

            //Assert
            Assert.AreEqual("Billy Ocean - Another Day Won't Matter", target);
        }
    }
}