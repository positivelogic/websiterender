﻿$binRoot = (Get-Item -Path ".\" -Verbose).FullName
Write-Host "binRoot:"$binRoot;
$packageRoot = (Get-Item -Path "..\..\..\..\packages" -Verbose).FullName;
Write-Host "packageRoot:"$packageRoot;
$openCoverExe = [io.path]::combine($packageRoot,"OpenCover.4.6.519\tools\OpenCover.Console.exe");
Write-Host "openCoverExe:"$openCoverExe;
$nunitConsoleExe = [io.path]::combine($packageRoot,"NUnit.ConsoleRunner.3.7.0\tools\nunit3-console.exe");
Write-Host "nunitConsoleExe:"$nunitConsoleExe;
$codeCoverageFile = "_CodeCoverageResult.xml";
Write-Host "codeCoverageFile:"$codeCoverageFile;
$codecov = [io.path]::combine($packageRoot,"Codecov.1.0.3\tools\codecov.exe");
Write-Host "codecov:"$codecov;

&$openCoverExe -register:user -target:$nunitConsoleExe -targetargs:"/noheader MediaSaver.UnitTests.dll /shadowcopy" -filter:"+[MediaSaver]MediaSaver*" -excludebyattribute:"System.CodeDom.Compiler.GeneratedCodeAttribute" -register:user -output:$codeCoverageFile;
&$codecov -f $codeCoverageFile