﻿using NUnit.Framework;
using System.Threading;
using System.Threading.Tasks;
using MediaSaver.Services;

namespace MediaSaver.UnitTests.Services
{
    [TestFixture]
    public class YoutubeInMp3DownloadServicesTests
    {
        //[Test]
        public async Task Call_SaveAudioAsync_Result_Ok()
        {
            var target = new YoutubeInMp3DownloadService(60, 9999999);
            var link = "https://www.youtube.com/watch?v=ZxalEA53008";
            var path = @"C:\Temp\Audio";
            var cancelToken = new CancellationTokenSource();

            await target.SaveAudioAsync(link, path, cancelToken);
        }
    }
}