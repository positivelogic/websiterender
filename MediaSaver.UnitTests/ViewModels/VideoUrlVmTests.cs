﻿using System.Threading;

using Moq;
using NUnit.Framework;
using Unity;

using MediaSaver.ViewModels;
using MediaSaver.UnitTests.Mocks;
using MediaSaver.Shared.ConfigurationManagement;
using MediaSaver.Shared.StorageManagement;
using MediaSaver.Shared.UI;
using MediaSaver.Services.Client;
using MediaSaver.Services;
using MediaSaver.Entities;

namespace MediaSaver.UnitTests.ViewModels
{
    [TestFixture]
    public class VideoUrlVmTests
    {
        class Arrangement
        {
            public MoqSiteInfoService MoqSiteInfoService;
            public MoqSearchServices MoqSearchService;
            public MoqThreadingServices MoqThreadingServices;
            public MoqLoggingService MoqLoggingService;
            public MoqYoutubDownloadService MoqYoutubeVideoDownloadService;
            public MoqYoutubeApiSearchService MoqYoutubeApiSearchService;
            public MoqVideoConversionService MoqVideoConversionService;
            public MoqYoutubeInMp3DownloadService MoqYoutubeInMp3DownloadService;
            public MoqMainVm MoqMainVm;
            public MoqSettingsVm MoqSettingsVm;
            public Arrangement()
            {
                App.Container = App.CreateUnityContainer();


                MoqSiteInfoService = new MoqSiteInfoService();
                MoqSearchService = new MoqSearchServices();
                MoqThreadingServices = new MoqThreadingServices();
                MoqLoggingService = new MoqLoggingService();
                MoqYoutubeVideoDownloadService = new MoqYoutubDownloadService();
                MoqYoutubeApiSearchService = new MoqYoutubeApiSearchService();
                MoqVideoConversionService = new MoqVideoConversionService();
                MoqYoutubeInMp3DownloadService = new MoqYoutubeInMp3DownloadService();

                MoqMainVm = new MoqMainVm();
                MoqSettingsVm = new MoqSettingsVm();

                App.Container.RegisterInstance<ISettingsVm>(MoqSettingsVm.Object);
                App.Container.RegisterInstance<IYoutubeApiSearchService>(MoqYoutubeApiSearchService.Object);
                App.Container.RegisterInstance<ISearchServices>(MoqSearchService.Object);
                App.Container.RegisterInstance<ISiteInfoService>(MoqSiteInfoService.Object);
                App.Container.RegisterInstance<IDownloadService>(IocEnums.YoutubeExtractor, MoqYoutubeVideoDownloadService.Object);
                App.Container.RegisterInstance<IVideoConversionService>(MoqVideoConversionService.Object);
                App.Container.RegisterInstance<IDownloadService>(IocEnums.YoutubeInMp3, MoqYoutubeInMp3DownloadService.Object);
                App.Container.RegisterInstance<IMainVm>(MoqMainVm.Object);
            }

            public VideoUrlVm SetupNewTarget()
            {
                return new VideoUrlVm(MoqYoutubeVideoDownloadService.Object, 
                                        MoqVideoConversionService.Object,
                                            MoqYoutubeInMp3DownloadService.Object,
                                                MoqMainVm.Object,
                                                    MoqSettingsVm.Object,
                                                        MoqLoggingService.Object);
            }
        }


        [Test]
        public void Call_SaveVideoCommand_When_Url_Null_Do_Not_Call_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();
            
            //Act
            var target = arrange.SetupNewTarget();
            target.Url = null;
            target.SaveVideoCommand.Execute(null);

            //Assert
            arrange.MoqYoutubeVideoDownloadService.Verify(m => m.SaveVideoAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Never());
        }
        [Test]
        public void Call_SaveVideoCommand_When_Url_Empty_Do_Not_Call_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();

            //Act
            var target = arrange.SetupNewTarget();
            target.Url = string.Empty;
            target.SaveVideoCommand.Execute(null);

            //Assert
            arrange.MoqYoutubeVideoDownloadService.Verify(m => m.SaveVideoAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Never());
        }
        [Test]
        public void Call_SaveVideoCommand_When_Url_Spaces_Do_Not_Call_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();

            //Act
            var target = arrange.SetupNewTarget();
            target.Url = " ";
            target.SaveVideoCommand.Execute(null);

            //Assert
            arrange.MoqYoutubeVideoDownloadService.Verify(m => m.SaveVideoAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Never());
        }
        [Test]
        public void Call_SaveVideoCommand_When_Url_Do_Call_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();

            //Act
            var target = arrange.SetupNewTarget();
            target.Url = "some url";
            target.SaveVideoCommand.Execute(null);

            //Assert
            arrange.MoqYoutubeVideoDownloadService.Verify(m => m.SaveVideoAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()), Times.Once());
        }
        [Test]
        public void Call_SaveAudioCommand_When_AudioOnlyDownload_True_Do_Call_YoutubeInMp3DownloadServices_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSettingsVm.AudioOnlyDownload = true;

            //Act
            var target = arrange.SetupNewTarget();
            target.Url = "some url";
            target.SaveAudioCommand.Execute(null);

            //Assert
            arrange.MoqVideoConversionService.Verify(m => m.ConvertAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Never());
            arrange.MoqYoutubeInMp3DownloadService.Verify(m => m.SaveAudioAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationTokenSource>(), It.IsAny<string>()), Times.Once());
        }
        [Test]
        public void Call_SaveAudioCommand_When_AudioOnlyDownload_False_Do_Not_Call_YoutubeInMp3DownloadServices_SaveVideoAsync()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSettingsVm.AudioOnlyDownload = false;

            //Act
            var target = arrange.SetupNewTarget();
            target.Url = "some url";
            target.SaveAudioCommand.Execute(null);

            //Assert
            arrange.MoqVideoConversionService.Verify(m => m.ConvertAsync(It.IsAny<string>(), It.IsAny<string>()), Times.Once());
            arrange.MoqYoutubeInMp3DownloadService.Verify(m => m.SaveAudioAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<CancellationTokenSource>(), It.IsAny<string>()), Times.Never());
        }
    }
}