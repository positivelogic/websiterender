﻿using Moq;
using NUnit.Framework;
using MediaSaver.ViewModels;
using MediaSaver.UnitTests.Mocks;

namespace MediaSaver.UnitTests.ViewModels
{
    [TestFixture]
    public class FilesVmTests
    {
        [Test]
        public void Call_Constructor_Call_LoadFileInfoAsync()
        {
            //Arrange
            var moqThreadingServices = new MoqThreadingServices();
            var moqFileService = new MoqFileService();


            //Act
            var target = new FilesVm(moqThreadingServices.Object, moqFileService.Object);


            //Assert
            moqFileService.Verify(m => m.LoadFileInfoAsync(), Times.Once);
        }
        [Test]
        public void Call_CloseCommand_Call_CloseMe()
        {
            //Arrange
            var moqThreadingServices = new MoqThreadingServices();
            var moqFileService = new MoqFileService();
            var moqView = new MoqView();


            var target = new FilesVm(moqThreadingServices.Object, moqFileService.Object);
            target.View = moqView.Object;


            //Act
            target.CloseCommand.Execute(null);


            //Assert
            moqView.Verify(m => m.CloseMe(), Times.Once);
        }
    }
}