﻿using System;

using Moq;
using NUnit.Framework;

using MediaSaver.Entities;
using MediaSaver.ViewModels;
using MediaSaver.UnitTests.Mocks;

namespace MediaSaver.UnitTests.ViewModels
{
    [TestFixture]
    public class SiteInfoVmTests
    {
        [Test]
        public void Call_Contructor_Call_Load_No_SiteInfo_Throws_And_Show_Error()
        {
            //Arrange
            var moqSiteInfoService = new MoqSiteInfoService();
            var moqCustomMessageBoxService = new MoqCustomMessageBoxService();

            //Act
            var target = new SiteInfoVm(moqSiteInfoService.Object, moqCustomMessageBoxService.Object);

            //Assert
            moqCustomMessageBoxService.Verify(m => m.ShowError(It.Is<Exception>(er => er.Message == "No site info loaded." && er.GetType() == typeof(ApplicationException))), Times.Once);
        }
        [Test]
        public void Call_Contructor_Call_Load_With_SiteInfo_No_Error_Shown()
        {
            //Arrange
            var moqSiteInfoService = new MoqSiteInfoService();
            moqSiteInfoService.LoadDataToReturn = new System.Collections.Generic.List<SiteInfo>();
            moqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { });
            var moqCustomMessageBoxService = new MoqCustomMessageBoxService();

            //Act
            var target = new SiteInfoVm(moqSiteInfoService.Object, moqCustomMessageBoxService.Object);

            //Assert
            moqSiteInfoService.Verify(m => m.Load(), Times.Once());
            moqCustomMessageBoxService.Verify(m => m.ShowError(It.IsAny<Exception>()), Times.Never());
        }
        [Test]
        public void Call_SaveCommand_When_CanSave_True_And_SiteInfoList_Have_Data_Call_Save_Once()
        {
            //Arrange
            var moqSiteInfoService = new MoqSiteInfoService();
            var moqCustomMessageBoxService = new MoqCustomMessageBoxService();
            moqSiteInfoService.LoadDataToReturn = new System.Collections.Generic.List<SiteInfo>();
            moqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { });

            //Act
            var target = new SiteInfoVm(moqSiteInfoService.Object, moqCustomMessageBoxService.Object);
            target.CanSave = true;
            target.SaveCommand.Execute(null);

            //Assert
            moqSiteInfoService.Verify(m => m.Save(It.IsAny<System.Collections.Generic.List<SiteInfo>>()), Times.Once());
        }
        [Test]
        public void Call_SaveCommand_When_CanSave_False_And_SiteInfoList_Have_Data_Call_Save_Never()
        {
            //Arrange
            var moqSiteInfoService = new MoqSiteInfoService();
            var moqCustomMessageBoxService = new MoqCustomMessageBoxService();
            var moqView = new MoqView();
            moqSiteInfoService.LoadDataToReturn = new System.Collections.Generic.List<SiteInfo>();
            moqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { });

            //Act
            var target = new SiteInfoVm(moqSiteInfoService.Object, moqCustomMessageBoxService.Object);
            target.CanSave = false;
            target.View = moqView.Object;
            target.SaveCommand.Execute(null);

            //Assert
            moqSiteInfoService.Verify(m => m.Save(It.IsAny<System.Collections.Generic.List<SiteInfo>>()), Times.Never());
        }

        [Test]
        public void Call_SaveCommand_When_CanSave_False_And_SiteInfoList_No_Data_Throws()
        {
            //Arrange
            var moqSiteInfoService = new MoqSiteInfoService();
            var moqCustomMessageBoxService = new MoqCustomMessageBoxService();
            var moqView = new MoqView();

            //Act
            var target = new SiteInfoVm(moqSiteInfoService.Object, moqCustomMessageBoxService.Object);
            target.CanSave = false;
            target.View = moqView.Object;
            target.SaveCommand.Execute(null);

            //Assert
            moqSiteInfoService.Verify(m => m.Save(It.IsAny<System.Collections.Generic.List<SiteInfo>>()), Times.Never());
            moqCustomMessageBoxService.Verify(m => m.ShowError(It.Is<Exception>(er=>er.Message == "No site info loaded." && er.GetType() == typeof(ApplicationException))), Times.Once);
        }
    }
}
