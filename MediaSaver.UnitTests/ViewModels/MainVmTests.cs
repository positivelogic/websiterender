﻿using System;
using System.Threading.Tasks;

using Moq;
using NUnit.Framework;
using NUnit.Framework.Constraints;

using MediaSaver.ViewModels;
using MediaSaver.Entities;
using MediaSaver.UnitTests.Mocks;

namespace MediaSaver.UnitTests.ViewModels
{
    [TestFixture]
    public class MainVmTests
    {
        class Arrangement
        {
            public MoqSiteInfoService MoqSiteInfoService;
            public MoqSearchServices MoqSearchService;
            public MoqThreadingServices MoqThreadingServices;
            public MoqLoggingService MoqLoggingService;
            public MoqYoutubDownloadService MoqYoutubeVideoDownloadService;
            public MoqYoutubeApiSearchService MoqYoutubeApiSearchService;
            public MoqVideoConversionService MoqVideoConversionService;
            public MoqYoutubeInMp3DownloadService MoqYoutubeInMp3DownloadService;
            public MoqSettingsVm MoqSettingsVm;
            public MoqView MoqView;
            public Arrangement()
            {
                MoqSiteInfoService = new MoqSiteInfoService();
                MoqSearchService = new MoqSearchServices();
                MoqThreadingServices = new MoqThreadingServices();
                MoqLoggingService = new MoqLoggingService();
                MoqYoutubeVideoDownloadService = new MoqYoutubDownloadService();
                MoqYoutubeApiSearchService = new MoqYoutubeApiSearchService();
                MoqVideoConversionService = new MoqVideoConversionService();
                MoqYoutubeInMp3DownloadService = new MoqYoutubeInMp3DownloadService();
                MoqSettingsVm = new MoqSettingsVm();
                MoqView = new MoqView();
                MediaSaver.App.Container = MediaSaver.App.CreateUnityContainer();
            }

            public MainVm SetupNewTarget()
            {
                var vm = new MainVm(MoqSiteInfoService.Object, 
                                        MoqSearchService.Object, 
                                            MoqThreadingServices.Object, 
                                                MoqLoggingService.Object, 
                                                    MoqYoutubeVideoDownloadService.Object, 
                                                        MoqYoutubeApiSearchService.Object, 
                                                            MoqVideoConversionService.Object,
                                                                MoqYoutubeInMp3DownloadService.Object);
                vm.View = MoqView.Object;
                return vm;
            }
        }

        [Test]
        public void Call_Contructor_SiteInfoService_Have_Not_Itens_Throws()
        {
            //Arrange
            var arrange = new Arrangement();

            //Act
            ActualValueDelegate<object> testDelegate = () => arrange.SetupNewTarget();

            //Assert
            Assert.That(testDelegate, Throws.TypeOf<ApplicationException>().And.Message.Contain("No siteinfo retrieved."));
            arrange.MoqLoggingService.Verify(m => m.LogError(It.Is<Exception>(p => p.Message == "No siteinfo retrieved." && p.GetType() == typeof(ApplicationException)), It.IsAny<string>()), Times.Once);
        }
        [Test]
        public void Call_Contructor_SiteInfoService_Load_Called_Once()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site1.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site2.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com" });

            //Act
            var target = arrange.SetupNewTarget();

            //Assert
            arrange.MoqSiteInfoService.Verify(m => m.Load(), Times.Once());
        }
        [Test]
        public void Call_Contructor_Selects_First_Active_SiteInfo_Item()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site1.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site2.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com" });

            //Act
            var target = arrange.SetupNewTarget();

            //Assert
            Assert.That(target.SelectedSiteInfo.BaseUrl == "http://Site2.com");
        }
        [Test]
        public void Call_Contructor_When_No_Active_SiteInfo_Throws()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site1.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site2.com" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com" });

            //Act
            ActualValueDelegate<object> testDelegate = () => arrange.SetupNewTarget();

            //Assert
            Assert.That(testDelegate, Throws.TypeOf<ArgumentNullException>().And.Message.Contain("FirstActiveSiteInfo"));
            arrange.MoqLoggingService.Verify(m => m.LogError(It.Is<Exception>(p => p.Message == "Value cannot be null.\r\nParameter name: FirstActiveSiteInfo" && p.GetType() == typeof(ArgumentNullException)), It.IsAny<string>()), Times.Once);
        }
        [Test]
        public async Task Call_SearchAsync_When_No_SearchCriteria_Do_Not_Call_Search()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site1.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site2.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com", SearchUrl = "/results?search_query={0}" });

            //Act
            var target = arrange.SetupNewTarget();
            target.SearchCriteria = string.Empty;
            ActualValueDelegate<object> testDelegate = () => target;
            await target.SearchAsync();

            //Assert
            Assert.That(target.CanSearch, Is.EqualTo(false));
            arrange.MoqSearchService.Verify(m => m.Search(It.IsAny<SiteInfo>(), It.IsAny<string>()), Times.Never());
        }
        [Test]
        public async Task Call_SearchAsync_When_No_SearchCriteria_Do_Not_Call_RefreshMe()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site1.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site2.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com", SearchUrl = "/results?search_query={0}" });

            //Act
            var target = arrange.SetupNewTarget();
            target.SearchCriteria = string.Empty;
            ActualValueDelegate<object> testDelegate = () => target;
            await target.SearchAsync();

            //Assert
            arrange.MoqView.Verify(m => m.RefreshMe(), Times.Never);
        }
        [Test]
        public async Task Call_SearchAsync_When_SearchCriteria_Call_Search()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site1.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site2.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com", SearchUrl = "/results?search_query={0}" });

            //Act
            var target = arrange.SetupNewTarget();
            target.SearchCriteria = "Some search criteria";
            ActualValueDelegate<object> testDelegate = () => target;
            await target.SearchAsync();

            //Assert
            Assert.That(target.CanSearch, Is.EqualTo(true));
            arrange.MoqSearchService.Verify(m => m.Search(It.IsAny<SiteInfo>(), It.IsAny<string>()), Times.Never());
            arrange.MoqYoutubeApiSearchService.Verify(m => m.SearchAsync(It.IsAny<string>()), Times.Once());
            arrange.MoqView.Verify(m => m.RefreshMe(), Times.Once);
        }
        [Test]
        public async Task Call_SearchAsync_When_SearchCriteria_Do_Call_RefreshMe()
        {
            //Arrange
            var arrange = new Arrangement();
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = true, BaseUrl = "http://Site1.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site2.com", SearchUrl = "/results?search_query={0}" });
            arrange.MoqSiteInfoService.LoadDataToReturn.Add(new SiteInfo { Active = false, BaseUrl = "http://Site3.com", SearchUrl = "/results?search_query={0}" });

            //Act
            var target = arrange.SetupNewTarget();
            target.SearchCriteria = "Some search criteria";
            ActualValueDelegate<object> testDelegate = () => target;
            await target.SearchAsync();

            //Assert
            arrange.MoqView.Verify(m => m.RefreshMe(), Times.Once);
        }
    }
}