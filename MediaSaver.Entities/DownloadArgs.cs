﻿using System;

namespace MediaSaver.Entities
{
    public class DownloadArgs : EventArgs
    {
        public double ProgressPercentage { get; set; }
    }
}