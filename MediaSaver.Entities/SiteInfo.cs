﻿namespace MediaSaver.Entities
{
    public class SiteInfo
    {
        public bool Active { get; set; }
        public string Name { get; set; }
        public string BaseUrl { get; set; }
        public string SearchUrl { get; set; }
        public string SearchPrevResultFilter { get; set; }
        public string SearchNextResultFilter { get; set; }
        public string SearchResultFilter { get; set; }
        public string CoreAttribute { get; set; }
        public string CoreDescription1 { get; set; }
        public char MorePageSplitter { get; set; }
    }
}