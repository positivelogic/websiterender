﻿namespace MediaSaver.Entities
{
    public class IocEnums
    {
        public const string YoutubeExtractor = "YoutubeExtractor";
        public const string VideoLibrary = "VideoLibrary";
        public const string YoutubeInMp3 = "YoutubeInMp3";
        public const string SearchServices = "SearchServices";
        public const string MainVm = "MainVm";
    }
}
