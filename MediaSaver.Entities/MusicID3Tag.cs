﻿namespace MediaSaver.Entities
{
    public class MusicID3Tag
    {
        public byte[] TAGID = new byte[3];      //  3
        public byte[] Title = new byte[30];     //  30
        public byte[] Artist = new byte[30];    //  30 
        public byte[] Album = new byte[30];     //  30 
        public byte[] Year = new byte[4];       //  4 
        public byte[] Comment = new byte[30];   //  30 
        public byte[] Genre = new byte[1];      //  1
    }
}

//if (mp3FileStream.Length >= 128)
//{
//    MusicID3Tag tag = new MusicID3Tag();
//    mp3FileStream.Seek(-128, SeekOrigin.End);
//    mp3FileStream.Read(tag.TAGID, 0, tag.TAGID.Length);
//    mp3FileStream.Read(tag.Title, 0, tag.Title.Length);
//    mp3FileStream.Read(tag.Artist, 0, tag.Artist.Length);
//    mp3FileStream.Read(tag.Album, 0, tag.Album.Length);
//    mp3FileStream.Read(tag.Year, 0, tag.Year.Length);
//    mp3FileStream.Read(tag.Comment, 0, tag.Comment.Length);
//    mp3FileStream.Read(tag.Genre, 0, tag.Genre.Length);
//    string theTAGID = Encoding.Default.GetString(tag.TAGID);

//    if (theTAGID.Equals("TAG"))
//    {
//        string Title = Encoding.Default.GetString(tag.Title);
//        string Artist = Encoding.Default.GetString(tag.Artist);
//        string Album = Encoding.Default.GetString(tag.Album);
//        string Year = Encoding.Default.GetString(tag.Year);
//        string Comment = Encoding.Default.GetString(tag.Comment);
//        string Genre = Encoding.Default.GetString(tag.Genre);

//        Console.WriteLine(Title);
//        Console.WriteLine(Artist);
//        Console.WriteLine(Album);
//        Console.WriteLine(Year);
//        Console.WriteLine(Comment);
//        Console.WriteLine(Genre);
//        Console.WriteLine();
//    }
//}