﻿namespace MediaSaver.Entities
{
    /// <summary>
    //  The videoDefinition parameter lets you restrict a search to only include either
    //  high definition (HD) or standard definition (SD) videos. HD videos are available
    //  for playback in at least 720p, though higher resolutions, like 1080p, might also
    //  be available. If you specify a value for this parameter, you must also set the
    //  type parameter's value to video.
    /// </summary>
    public enum InternalVideoDefinitionEnum
    {
        /// <summary>
        /// Return all videos, regardless of their resolution.
        /// </summary>
        Any = 0,
        /// <summary>
        /// Only retrieve HD videos.
        /// </summary>
        High = 1,
        /// <summary>
        /// Only retrieve videos in standard definition.
        /// </summary>
        Standard = 2
    }
}