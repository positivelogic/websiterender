﻿namespace MediaSaver.Entities
{
    public class Info
    {
        public string site { get; set; }
        public bool ytCypherUsed { get; set; }
        public string url_browser { get; set; }
        public int xxx { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string metadesc { get; set; }
        public string metakw { get; set; }
        public string[] keywords { get; set; }
        public string author { get; set; }
        public int view_count { get; set; }
        public Link[] link { get; set; }
        public string thumb { get; set; }
    }

    public class Link
    {
        public string url { get; set; }
        public string quality { get; set; }
        public string mime { get; set; }
        public string mime_short { get; set; }
    }
}