﻿using System;
using System.IO;

namespace MediaSaver.Entities
{
    public class FileInfoModel
    {
        public int Number { get; set; }
        public DateTime CreationTime { get; set; }
        public string Extension { get; set; }
        public string FullName { get; set; }
        public long Length { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}