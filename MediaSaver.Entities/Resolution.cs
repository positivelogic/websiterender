﻿namespace MediaSaver.Entities
{
    public enum Resolution
    {
        One = 0,
        Two = 144,
        Three = 240,
        Four = 360,
        Five = 480,
        Six = 720,
        Seven = 1080
    }
}