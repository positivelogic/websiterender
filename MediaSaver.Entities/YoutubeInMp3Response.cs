﻿namespace MediaSaver.Entities
{

    public class YoutubeInMp3Response
    {
        public string title { get; set; }
        public string length { get; set; }
        public string link { get; set; }
    }
}