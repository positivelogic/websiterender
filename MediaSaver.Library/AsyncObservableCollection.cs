﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;

namespace MediaSaver.Library
{
    public class AsyncObservableCollection<T> : ObservableCollection<T>
    {
        private readonly SynchronizationContext _synchronizationContext = SynchronizationContext.Current;
        private readonly Dispatcher _dispatcher;

        public AsyncObservableCollection(Dispatcher dispatcher)
            : this(dispatcher, SynchronizationContext.Current)
        {
        }

        public AsyncObservableCollection(Dispatcher dispatcher, SynchronizationContext synchronizationContext)
        {
            _dispatcher = dispatcher;
            _synchronizationContext = synchronizationContext;
        }

        private void ExecuteOnSyncContext(Action action)
        {
            if (SynchronizationContext.Current == _synchronizationContext)
            {
                action();
            }
            else
            {
                _synchronizationContext.Send(_ => action(), null);
            }
        }

        protected override void InsertItem(int index, T item)
        {
            if (_dispatcher != null)
            {
                _dispatcher.Invoke((Action)delegate
                {
                    ExecuteOnSyncContext(() => base.InsertItem(index, item));
                });
            }
            else
            {
                ExecuteOnSyncContext(() => base.InsertItem(index, item));
            }
        }

        protected override void RemoveItem(int index)
        {
            if (_dispatcher != null)
            {
                _dispatcher.Invoke((Action)delegate
                {
                    ExecuteOnSyncContext(() => base.RemoveItem(index));
                });
            }
            else
            {
                ExecuteOnSyncContext(() => base.RemoveItem(index));
            }
        }

        protected override void SetItem(int index, T item)
        {
            if (_dispatcher != null)
            {
                _dispatcher.Invoke((Action)delegate
                {
                    ExecuteOnSyncContext(() => base.SetItem(index, item));
                });
            }
            else
            {
                ExecuteOnSyncContext(() => base.SetItem(index, item));
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            if (_dispatcher != null)
            {
                _dispatcher.Invoke((Action)delegate
                {
                    ExecuteOnSyncContext(() => base.MoveItem(oldIndex, newIndex));
                });
            }
            else
            {
                ExecuteOnSyncContext(() => base.MoveItem(oldIndex, newIndex));
            }
        }

        protected override void ClearItems()
        {
            if (_dispatcher != null)
            {
                _dispatcher.Invoke((Action)delegate
                {
                    ExecuteOnSyncContext(() => base.ClearItems());
                });
            }
            else
            {
                ExecuteOnSyncContext(() => base.ClearItems());
            }
        }
    }
}