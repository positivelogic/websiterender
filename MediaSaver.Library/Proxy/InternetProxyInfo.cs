﻿namespace MediaSaver.Library.Proxy
{
    public struct InternetProxyInfo
    {
        public InternetOpenType AccessType;
        public string ProxyAddress;
        public string ProxyBypass;
    }
}
