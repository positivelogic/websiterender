﻿using System;

namespace MediaSaver.Library.CustomEventArgs
{
    public class VideoConversionProgressArgs : EventArgs
    {
        public double? Bitrate { get; set; }
        public double Fps { get; set; }
        public long Frame { get; set; }
        public TimeSpan ProcessedDuration { get; set; }
        public int SizeKb { get; set; }
        public TimeSpan TotalDuration { get; set; }
    }
}