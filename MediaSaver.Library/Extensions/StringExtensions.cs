﻿using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace MediaSaver.Library.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveIllegalChars(this string value)
        {
            //string illegal = "\"M\"\\a/ry/ h**ad:>> a\\/:*?\"| li*tt|le|| la\"mb.?";
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            if (string.IsNullOrEmpty(value)) return string.Empty;

            foreach (char c in invalid)
            {
                value = value.Replace(c.ToString(), "");
            }

            value = value.ToTitleCase();

            return value;
        }

        public static string ToTitleCase(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            TextInfo textInfo = CultureInfo.CurrentCulture.TextInfo;
            value = textInfo.ToTitleCase(value.ToLower());
            return value;
        }

        public static string TrimIfNull(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            if (string.IsNullOrEmpty(value.Trim())) return string.Empty;
            return value.Trim();
        }

        private static readonly Regex RemovalRegex = new Regex(@"[()\d]");
        public static string RemoveUnwantedCharacters(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var result = RemovalRegex.Replace(value, string.Empty);
            return result.Trim();
        }

        public static string ReplaceWholeWord(this string s, string word, string bywhat)
        {
            char firstLetter = word[0];
            StringBuilder sb = new StringBuilder();
            bool previousWasLetterOrDigit = false;
            int i = 0;
            while (i < s.Length - word.Length + 1)
            {
                bool wordFound = false;
                char c = s[i];
                if (c == firstLetter)
                    if (!previousWasLetterOrDigit)
                        if (s.Substring(i, word.Length).Equals(word))
                        {
                            wordFound = true;
                            bool wholeWordFound = true;
                            if (s.Length > i + word.Length)
                            {
                                if (char.IsLetterOrDigit(s[i + word.Length]))
                                    wholeWordFound = false;
                            }

                            if (wholeWordFound)
                                sb.Append(bywhat);
                            else
                                sb.Append(word);

                            i += word.Length;
                        }

                if (!wordFound)
                {
                    previousWasLetterOrDigit = char.IsLetterOrDigit(c);
                    sb.Append(c);
                    i++;
                }
            }

            if (s.Length - i > 0)
                sb.Append(s.Substring(i));

            return sb.ToString();
        }
    }
}
