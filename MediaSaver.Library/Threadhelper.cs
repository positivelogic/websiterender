﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MediaSaver.Library
{
    public static class Threadhelper
    {
        public static Task StartSTATask(Action action)
        {
            TaskCompletionSource<object> source = new TaskCompletionSource<object>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    action();
                    source.SetResult(null);
                }
                catch (Exception ex)
                {
                    source.SetException(ex);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return source.Task;
        }

        public static Task<TResult> StartSTATask<TResult>(Func<TResult> function)
        {
            TaskCompletionSource<TResult> source = new TaskCompletionSource<TResult>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    source.SetResult(function());
                }
                catch (Exception ex)
                {
                    source.SetException(ex);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return source.Task;
        }
    }
}